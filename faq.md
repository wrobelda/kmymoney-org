---
title: "Frequently Asked Questions"
layout: page
questions:
  - question: "How do I compile from Git?"
    answer: >
        [Our "Installing From Git" page](https://techbase.kde.org/Projects/KMyMoney#Installation)
        contains all the information about compiling and installing from Git.

  - question: "Where do I find some more information about OFX parameters?"
    answer: >
        Here are some external sources which might give your more information:
        [ofxblog](http://ofxblog.wordpress.com/) and [ofxhome](http://www.ofxhome.com/).

  - question: "Why is a transaction shown in red and how can I get rid of the color?"
    answer: |
        The transaction is shown in red if parts of the amount are not categorized.
        This is most commonly caused by importing a QIF or OFX file or by downloading
        statement data online.

        So all you have to do is to modify the transaction such that
        the whole amount of the transaction is assigned to
        categories.

  - question: "Why does the QIF import choke on a wrong date format?"
    answer: |
        The QIF specification does not specify a definite format for
        dates. Several different formats are used and the format is
        selected by the source of your QIF file (e.g. your bank). In
        order to cope with many different date formats, KMyMoney
        provides 'QIF profiles'. Each profile allows to customize
        KMyMoney to accept the input data format chosen by the
        originator of the QIF file.

        With respect to dates, KMyMoney supports m/d/y as well as
        d/m/y formats. 2 digit year info as well as 4 digit year info
        is also supported. For 2 digit year info some institutions use
        an apostrophe to identify a certain range of years. Within the
        date tab of the QIF profile editor you can also select the
        behaviour of KMyMoney in case of such an apostrophe.

        <center>
        <table border="1" summary="date formats">
        <tbody><tr>
        <td align="center"><b>Date Format</b></td>

        <td align="center"><b>Meaning</b></td>
        </tr>

        <tr>
        <td>%yy</td>

        <td>identifies two digit year info</td>
        </tr>

        <tr>
        <td>%yyyy</td>

        <td>identifies four digit year info</td>
        </tr>

        <tr>
        <td>%m</td>

        <td>identifies a numeric month (1-12)</td>
        </tr>

        <tr>
        <td>%mmm</td>

        <td>identifies a short month name (e.g. Jan, Feb)</td>
        </tr>

        <tr>
        <td>%d</td>

        <td>identifies a numeric day (1-31)</td>
        </tr>
        </tbody></table>
        </center>

        Example: If you see a date of 21/3\'03 in your records,
        select the '%d/%m/%yy' setting. It does not matter if you have
        a / or an ' preceeding the years. Therfore, '%d/%m%yy' would be
        the wrong format. If your bank sends you this file for the year
        2003 then you probably want to set the apostrophe handling to
        '2000-2099'. Then the above record would be interpreted to
        carry the '21st of March 2003' as it's date.

        If you picked the wrong date format in the QIF profile,
        KMyMoney cannot correctly interpret the data and shows error
        messages. The default in this case is to use today's date.

  - question: When I start KMyMoney I get a "Malformed URL" error. What's wrong?
    answer: >
        Usually the reason for this is that you didn't run `make install`
        as root. This step actually copies all of the icons,
        and other files needed by KMyMoney to run properly. If you did
        run this, make sure you passed the correct prefix to
        `configure` at the beginning of your compile.

  - question: >
        I have mistyped the name of a payee and want to change it
        in all transaction. Is there a global search/replace?

    answer: >
        There is no global search and replace function in KMyMoney.
        Nevertheless, it is easy to change a payees name: select the
        Payees view and search the entry with the name you want to
        change. Enter the new name. Since the transactions only use a
        reference to the payees record, all transactions now show the
        new name.

  - question: Why is there no translation for 'my' language"
    answer: >
        Most likely, the reason is that no one has offered to
        translate it yet. Translating KDE/QT-based applications is very
        simple, and does not require any programming skills. If you
        would like to translate KMyMoney into your language, please
        post a message to the [KMyMoney](mailto:kmymoney-devel@kde.org),
        and we will give you the details on how you can easily provide
        a translation for us.

  - question: When I compile from source, all the icons show up as missing. What did I do wrong?
    answer: >
        Usually the reason for this is that you didn't run 'make
        install' as root. This step actually copies all of the icons,
        and other files needed by KMyMoney to run properly. If you did
        run this, make sure you passed the correct prefix to
        'configure' at the beginning of your compile.

  - question: How can I use distcc to help speed up compilation of KMyMoney, or other KDE (C++) applications?
    answer: |
        <a href="http://distcc.samba.org">distcc</a> is a tool
        developed from the Samba team to help distribute compiling of
        applications on multiple machines at once. You have to set up
        distcc on each machine that you want to compile on, and distcc
        takes care of distributing the compile tasks to all of the
        various machines. <a href="http://www-106.ibm.com/developerworks/linux/library/l-distcc.html?ca=dgr-lnxw07Distcc">
        An article at ibm.com</a> is what got me started, and cut down
        my compile times by a factor of 8.

        After I first installed distcc, I couldn\'t get the distributed
        compiling to work. I stumbled upon <a href="http://forums.gentoo.org/viewtopic.php?t=22092">this post</a>
        in the gentoo forums which told me how to get distcc to work
        with C++ applications. After I created the /usr/bin/dist++
        shell script, (and performing an \'export CXX=dist++\'), the next
        time I ran make on KMyMoney, it was being compiled on two
        machines.

        My compile with distcc was about 8 times as fast as compiling
        just on my local machine. My local machine is a Athlon XP 1700,
        512 MB ram. Compiling with just the local machine took 17m51s
        after running make clean on my current codebase. I added my
        second computer, a 2.4 GHz Celeron, with 384 MB ram, and the
        compile time shrank to 2m45s.

        Another useful link on this topic is at <a href="http://www.osnews.com/comment.php?news_id=7460">OSNews.com</a>.

  - question: 'Speaking of names: where did the "2" come from? Is it KMyMoney or KMyMoney2?'
    answer: >
        The name of the app is KMyMoney. The "2" is historical
        legacy from when the project went from KDE 1 to KDE 2. You can
        search the mailing list archive for more details.

  - question: The Git version is broken! What do I do now?
    answer: |
        Usually the reason for this is that one of the developers
        made a change to the code that requires a clean reconfigure.
        Please run a \'git pull --rebase\' to update your local repository.

        After following these steps carefully, if there is still a
        problem compiling, please write the <a href="mailto:kmymoney-devel@kde.org">KMyMoney
        Developers List</a> and send in the failed output of
        "make".

  - question: >
        Accounts and categories show a total
        balance of 0.00 which is wrong. What's going on here?
    answer: |
        This is a design problem in the 0.8 branch which has been
        fixed in the current development version. It will show
        the total of the account and all its sub-ordinate accounts
        when the entry in the tree is collapsed. If the entry is
        expanded, the value shown will be the one of the account
        without the sub-oridinate accounts included.

        Due to design reasons, the fix cannot easily be backported
        to the 0.8 branch.

  - question: Will KMymoney work on my X/Ubuntu desktop?
    answer: >
        It will work, but you will have to install the `basic KDE libraries`
        first, as Xubuntu comes with Gnome libraries by default. If you want
        to install from Git, you should install <code>build-essential</code> and
        <code>kdelibs4-dev</code>. The second one will install a ton of other
        KDE-related packages.

  - question: How do I configure file encryption on Windows ?
    answer: >
        KMyMoney on Windows uses an embedded copy of GPG to support file encryption.
        To create a new gpg key pair open a command shell in the <b>bin</b> directory
        of the kmymoney installation and run: <code>gpg2 --gen-key</code>. Then, in kmymoney
        settings dialog, enter encryption tab, enable encryption support and choose the
        just generated gpg key. Now you are ready to save to an encrypted file and load from it.
        For additional gpg key management see [here](https://www.gnupg.org/gph/de/manual/x193.html).

  - question: How can I report a bug?
    answer: >
        You can report a bug <a href="https://bugs.kde.org/enter_bug.cgi?product=kmymoney&amp;format=guided">here</a>.
        You can also browse the [mailing list](https://mail.kde.org/pipermail/kmymoney-devel/)
        to search for previous discussions of similar bugs.

---

# FAQ

Hopefully this page will answer many of the questions a first
time user may have as well as answering many common problems
encountered while building and running KMyMoney.

## Questions

{% for question in page.questions %}
+ [{{ question.question }}](#{{forloop.index}})
{% endfor %}

## Answers

{% for question in page.questions %}
<span id="#{{forloop.index}}"></span>
### {{ question.question }} 

{{ question.answer|markdonify}}
{% endfor %}


## Anything else?

If you have a question that is not answered on this FAQ, please
don't hesitate to post it on the [KMyMoney Developers List](mailto:kmymoney-devel@kde.org),
the [KMyMoney Users List](mailto:kmymoney@kde.org) or the
[KMyMoney Forum](https://forum.kde.org/viewforum.php?f=69) and we will try to
answer you as soon as possible.


