<?php

$products = array(
    array(
        "KMyMoney5 (Aqbanking6)",
        ":/kmymoney5",
        "kmymoney5",
        "https://cgit.kde.org/kmymoney.git/log/?h=5.0"
    ),
    array(
        "KMyMoney4 (Aqbanking6)",
        ":/kmymoney",
        "kmymoney",
        "https://cgit.kde.org/kmymoney.git/log/?h=4.8"
    ),
    array(
        "KMyMoney4 (Aqbanking5)",
        ":/kmymoney-aq5",
        "kmymoney",
        "https://cgit.kde.org/kmymoney.git/log/?h=4.8"
    ),
    array(
        "KMyMoney4 (Aqbanking5,   staging branch)",
        ":/staging",
        "kmymoney",
        "https://github.com/rhabacker/kmymoney/commits/4.8-staging"
    ),
);

$archs = array( "32", "64");


function getUrl($product,$arch)
{
    return "https://download.opensuse.org/repositories/home:/rhabacker:/branches:/windows:/mingw:/win$arch$product/openSUSE_Leap_15.1/noarch/";
}

header("Content-Type: text/html; charset=utf-8");

echo "<!DOCTYPE html>"
    . "<html><body style=\"background-color:white;\">"
    . "<h1>Download cross compiled KMyMoney snapshots</h1>"
    ;

foreach($products as $product) {
    $productName = $product[0];
    $path = $product[1];
    $baseName = $product[2];
    $gitUrl = $product[3];
    echo "<h2>Product: $productName</h2>"
        ."<a href=\"".$gitUrl."\">Changelog</a><br/>"
        ."<br/>"
        ;

    foreach($archs as $arch) {
        $url = getUrl($path, $arch);
        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $page = curl_exec($ch);
            curl_close($ch);
            #echo "$url - $page";
            $lines = explode("\n", $page);
            $portableLink = "";
            $setupLink = "";
            $debugPackageLink = "";
            foreach($lines as $line) {
                #  href="mingw64-kmymoney-portable-4.8.3cf78f9c1-lp151.76.16.noarch.rpm"><..."
                preg_match("/<a href=\"(.*$baseName-.*\.rpm)\"></", $line, $matches);
                if (sizeof($matches) > 0) {
                    #echo "+++<pre>".$matches[1]."</pre>+++";
                    if (strstr($matches[1], "portable") !== FALSE)
                        $portableLink = $matches[1];
                    if (strstr($matches[1], "setup") !== FALSE)
                        $setupLink = $matches[1];
                    if (strstr($matches[1], "debugpackage") !== FALSE)
                        $debugPackageLink = $matches[1];
                }
            }
            echo "$arch bit<br/>";
            if ($portableLink != '') {
                echo "<ul>"
                    . "<li><a href=\"$url$portableLink\">$portableLink</a>&nbsp;Zip file with portable installation</li>"
                    . "<li><a href=\"$url$setupLink\">$setupLink</a>&nbsp;Setup installer</li>"
                    . "<li><a href=\"$url$debugPackageLink\">$debugPackageLink</a>&nbsp;Debug symbols [1]</li>"
                    . "</ul>"
                    ;
            } else {
                echo "<ul><li>No snapshots available yet.</li></ul>";
            }
        } else {
            echo "<ul>"
                . "<li>choose <b>mingw$arch-$baseName-portable-....rpm</b> to get a zip file with a portable installation of $productName</li>"
                . "<li>choose <b>mingw$arch-$baseName-setup-....rpm</b> to get a setup installer for $productName</li>"
                . "<li>Debug symbols can be found in the file <b>mingw$arch-$baseName-debugpackage-....rpm</b>. They need to be unpacked in the directory above the <b>bin</b> folder
                so that gdb can find them.</li>"
                . "</ul>"
                . "<p>After clicking on the button below you will be redirected to the related download page to get a $productName snapshot</p>"
                . "<input type=\"button\" name=\"xxx\" value=\"$arch-bit snapshot\" onclick=\"javascript:window.location='$url';\">"
                ;
        }
    }
}
echo "<p>The rpm container can be unpacked with <a href=\"https://www.7-zip.de/\">7-zip</a>.</p>"
     ."<p>[1]&nbsp;Debug symbols need to be unpacked in the directory above the <b>bin</b> folder so that gdb can find them.</p>"
     ."</body></html>"
    ;
