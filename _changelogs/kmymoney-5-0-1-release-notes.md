---
title: '        KMyMoney 5.0.1 Release Notes
      '
date: 2018-03-17 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.1/src/kmymoney-5.0.1.tar.xz.mirrorlist">version 5.0.1</a> of its open source Personal Finance Manager.</p><p>Although several members of the development team had been using the new
        version 5.0.0 in production for some time, a number of bugs and regressions slipped
        through testing, mainly in areas and features not used by them.</p><p>These have been reported by many of you and the development team worked hard
        to fix them in the meantime. Some of them are very annoying which lead us to
        work on a first maintenance release which is available immediately as KMyMoney 5.0.1.</p><p>Here is the list of the bugs which have been fixed:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390264">390264</a> Clicking autocomplete entry crashes application</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=350850">350850</a> Kmymoney does not open multiple files correctly</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=377760">377760</a> Cannot turn off Outbox tab in 4.8.0.</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390042">390042</a> can't change ledger sort options in configure dialog</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390176">390176</a> File/backup give mount/unmount error, even if "mount" is not checked</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390232">390232</a> Don't add new payee from new operation panel</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390627">390627</a> QIF Import filter fails</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390657">390657</a> Huge gap in report display screen</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390658">390658</a> Crash &gt; Tools &gt; Currencies</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390834">390834</a> Scheduled transactions: "Enter next transaction" should be available in ledger</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390873">390873</a> dollar symbol appears after amount</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390967">390967</a> quantity in Investments view should be right-justified</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391048">391048</a> Pre-Fill dialog missing</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391249">391249</a> Show tip of the day</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391453">391453</a> Displaying the ledger without an account having been selected displays a closed account</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391733">391733</a> Startup WM Class is wrong</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391770">391770</a> CSV importer ignores last line if not followed by a newline</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391773">391773</a> Context does not change to ledger view from the find transaction dialog</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390178">390178</a> After completing reconcile, Statement/Cleared/Balance show reconcile amounts, not account amounts</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390406">390406</a> switching from Investments page to Ledgers always shows same stale closed account ledger</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390593">390593</a> Clearing ledger search box doesn't return to the previous position</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390249">390249</a> Ctrl+Shift+Space shortcut no longer available</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390467">390467</a> Save as defaults to encrypted with no way to not encrypt</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=391961">391961</a> Home page Net Worth Forecast Y axis starts at 0</li>
          </ul>
        </p><p>Here are some of the new improvements found in this release:
          <ul>
            <li>Remove old locolor icons</li>
            <li>Install local icons in the local datadir</li>
            <li>Fix icon presentation of our own icons</li>
            <li>Fix build with QtWebEngine</li>
            <li>Enable investment actions when appropriate</li>
            <li>Ledger search box now supports the account hierarchy character ":"</li>
            <li>Enable calculator tool button</li>
            <li>Allow CSV importer to deal with Feb 30th</li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-5.0.1.txt">changelog</a>. We highly recommend upgrading to 5.0.1.</p><p>The KMyMoney Development Team</p>