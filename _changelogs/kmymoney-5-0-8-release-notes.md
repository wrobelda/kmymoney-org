---
title: KMyMoney 5.0.8 Release Notes
date: 2020/01/19
layout: post
---

<p>The KMyMoney development team is proud to present <a href="https://download.kde.org/stable/kmymoney/5.0.8/src/kmymoney-5.0.8.tar.xz.mirrorlist">version 5.0.8</a> of its open source Personal Finance Manager.</p>

<p>Here is the list of bugfixes:
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=326212">326212</a> Editing split memo when there is only one split does not update the record memo</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=361865">361865</a> In German, the dialog uses "share" if it is actually shares and/or bonds (i.e. securities).</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=395052">395052</a> No documentation available for csv exporter</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=398982">398982</a> Opening Balances in forecast are wrong</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=399364">399364</a> SKR03 account template does not contain a flag for an opening balance account</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=406403">406403</a> Tags within Split transaction not reporting correctly</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=411015">411015</a> Investment doesn't show correct value after switching investments</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=412429">412429</a> No display of transactions in the tags view when tags are specified in a split</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=413325">413325</a> Problem with Web addresses at financial institutions</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=413555">413555</a> Reconciliation of Credit Card</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=414333">414333</a> Investments - Edit and New investments - Unable to display online source list for Finance::Quote</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=414932">414932</a> Incorrect "opening balance" in daily and weekly "Investment Worth Graph"</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=415257">415257</a> Changing status of an investment transaction clears bank ID</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=415409">415409</a> False detection of a matching investment cash dividend transaction</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=415548">415548</a> The character &lt; as part of a matched transaction which was imported causes load to fail</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=415668">415668</a> Networth by month shows incorrect values for investment</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=415793">415793</a> Creating multiple tags in a row does not correctly adjust the name</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416052">416052</a> AppImage is missing the account/category templates</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416269">416269</a> Changed name of loan account is not saved</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416274">416274</a> Investment reports show incorrect annualized return percentage</li>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=416410">416410</a> Add a recuring transaction - loss of input if we forget the source account</li>
  </ul>
</p>
<p>Here is the list of the enhancements which have been added:
  <ul>
    <li><a href="https://bugs.kde.org/show_bug.cgi?id=415411">415411</a> No support for check forms with split protocol</li>
  </ul>
</p>
<p>A complete description of all changes can be found in the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.8.txt">changelog</a></p>.
