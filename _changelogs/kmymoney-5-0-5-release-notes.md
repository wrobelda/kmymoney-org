---
title: '        KMyMoney 5.0.5 Release Notes
      '
date: 2019-07-10 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.5/src/kmymoney-5.0.5.tar.xz.mirrorlist">version 5.0.5</a> of its open source Personal Finance Manager.</p><p>Here is the list of the bugs which have been fixed:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=352029">352029</a> Tag not shown in ledger if only an category is set</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=393752">393752</a> Budgeted vs. Actual report: Budgeted values broken if ticks selected to Bi-Monthly, Quarterly or Yearly</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=402672">402672</a> Can't edit loan account with institution of "Accounts with no institution assigned"</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=405206">405206</a> OFX import targets the wrong account</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=407021">407021</a> Show hidden accounts</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=407422">407422</a> Invalid online statement balance shown</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=407800">407800</a> 21st June 2019 shows as non-processing day when holiday calendar is configured</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=407902">407902</a> KMyMoney crashes in LEAP15.1</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=407982">407982</a> Categories with different currencies, no more rate</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=408026">408026</a> Crash during CSV import</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=408205">408205</a> Merging payees results in error and data loss if payee is in matched transaction</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=408494">408494</a> AqBanking File Import does not allow to select account</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=409098">409098</a> Gnucash importer ignores the accounts currencies</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=409428">409428</a> Converted amount not entered into exchange rate editor</li>
          </ul>
        </p><p>Here is the list of the enhancements which have been added:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=409089">409089</a> In Payees view, change "Default Account" tab to "Default Category"</li>
          </ul>
        </p><p>A complete description of all changes can be found in the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.5.txt">changelog</a></p>.