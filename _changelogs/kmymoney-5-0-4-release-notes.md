---
title: '        KMyMoney 5.0.4 Release Notes
      '
date: 2019-04-21 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.4/src/kmymoney-5.0.4.tar.xz.mirrorlist">version 5.0.4</a> of its open source Personal Finance Manager.</p><p>
One fix needs special mentioning: <a href="https://bugs.kde.org/show_bug.cgi?id=406608">406608</a>
which deals with customized budget reports. KMyMoney supports two types of budget reports. One only
containing the budget information and the other, more detailed report, providing also information
about the actual situation. In the past this caused all customized budget reports to contain the
detailed version. In case you encounter this scenario and want to see only the budget values in
your customized report, please delete the customized report and re-create from the base report to
fix the problem.
        </p><p>Here is the list of the bugs which have been fixed:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=368159">368159</a> Report Transactions by Payee omits transactions lacking category</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=390681">390681</a> OFX import and unrecognized &lt;FITID&gt; tag</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=392305">392305</a> Not all Asset accounts are shown during OFX import</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396225">396225</a> When importing a ofx/qif file, it does not show me all my accounts</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=396978">396978</a> Stable xml file output</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=400761">400761</a> Cannot open files on MacOS</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=401397">401397</a> kmymoney changes group permissions</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403745">403745</a> in import dialog, newly-created account doesn't appear in pulldown menu</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403825">403825</a> Transaction validity filter is reset when re-opening configuration</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403826">403826</a> Transactions without category assignment are not shown in report</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403885">403885</a> Buying / selling investments interest / fees round to 2 decimal places even when currency is to 6 decimal places</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403886">403886</a> No way to set/change investment start date in investment wizard</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=403955">403955</a> After an action, the cursor returns to top of page and does not remain in a similar position to when action was started</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=404156">404156</a> Can't select many columns as memo</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=404848">404848</a> Crash on "Enter Next Transcation"</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=405061">405061</a> No chart printing support</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=405329">405329</a> CPU loop reconciling if all transactions are cleared</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=405817">405817</a> CSV importer trailing lines are treated as absolute lines</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=405828">405828</a> Budget problems</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=405928">405928</a> Loss of inserted data in transaction planner</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406073">406073</a> Change of forecast method is not reflected in forecast view</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406074">406074</a> Unused setting "Forecast (history)" for home view</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406220">406220</a> Crash when deleting more than 5000 transactions at once</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406509">406509</a> "Find Transaction..." dialog focus is on "Help" button instead of "Find"</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406525">406525</a> Subtotals are not correctly aggregated when (sub-)categories have the same name</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406537">406537</a> Encrypted file cannot be saved as unencrypted</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406608">406608</a> Custom report based on Annual Budget incorrectly getting Actuals</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=406714">406714</a> Home view shows budget header twice</li>
          </ul>
        </p><p>Here is the list of the enhancements which have been added:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=341589">341589</a> Cannot assign tag to a split</li>
          </ul>
        </p>