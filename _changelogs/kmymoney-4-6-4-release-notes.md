---
title: '        KMyMoney 4.6.4 Release Notes
      '
date: 2013-10-06 00:00:00 
layout: post
---

<p>After more than a year from the last bugfix release the KMyMoney development team is pleased
          to announce the immediate availability of KMyMoney version 4.6.4. This version contains a few fixes for bugs found in <a href="release-notes.php#itemKMyMoney463ReleaseNotes">4.6.3</a>.</p><p>Here is a list of the most important changes since the last stable release:
          <ul>
            <li>Fixed the interaction with input method editors. <a href="https://bugs.kde.org/show_bug.cgi?id=320579">#320579</a>, <a href="https://bugs.kde.org/show_bug.cgi?id=272393">#272393</a> and <a href="https://bugs.kde.org/show_bug.cgi?id=272631">#272631</a></li>
            <li>Fixed a crash after editing a security <a href="https://bugs.kde.org/show_bug.cgi?id=309105">#309105</a></li>
            <li>The 'Use system colors' setting was made a default setting <a href="https://bugs.kde.org/show_bug.cgi?id=309010">#309010</a></li>
            <li>Fixed the rendering of a ledger entry when the ledger does not have focus</li>
            <li>Fixed the persitency of some header settings like in the 'Investments' view <a href="https://bugs.kde.org/show_bug.cgi?id=310260">#310260</a></li>
            <li>Fixed a crash when OFX update is cancelled while waiting for KWallet <a href="https://bugs.kde.org/show_bug.cgi?id=281728">#281728</a></li>
            <li>Fixed a crash cause by a transaction with an empty postdate <a href="https://bugs.kde.org/show_bug.cgi?id=310265">#310265</a></li>
            <li>Fixed a possible crash while mapping an OFX account <a href="https://bugs.kde.org/show_bug.cgi?id=296681">#296681</a></li>
            <li>Added definition for new Azerbaijani Manat <a href="https://bugs.kde.org/show_bug.cgi?id=307774">#307774</a></li>
            <li>Fixed the category selection actions in the find transactions dialog. <a href="https://bugs.kde.org/show_bug.cgi?id=313874">#313874</a></li>
            </ul></p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.6.4.txt">changelog</a>. We highly recommend upgrading to 4.6.4 as soon as possible.</p>