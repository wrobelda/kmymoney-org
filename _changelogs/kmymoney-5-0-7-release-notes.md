---
title: '        KMyMoney 5.0.7 Release Notes
      '
date: 2019-09-22 00:00:00 
layout: post
---

<p>The KMyMoney development team is proud to present <a href="http://download.kde.org/stable/kmymoney/5.0.7/src/kmymoney-5.0.7.tar.xz.mirrorlist">version 5.0.7</a> of its open source Personal Finance Manager.</p><p>This release becomes necessary due to the new regulations of the <a href="https://en.wikipedia.org/wiki/Payment_Services_Directive">PSD2</a> which affects the online capabilities
            for German users. To make KMyMoney compatible with them, especially the
            <a href="https://en.wikipedia.org/wiki/Strong_customer_authentication">strong customer authentication</a> part,
            KMyMoney had to be adapted to updated APIs of the Gwenhywfar and AqBanking libraries which provide the banking
            protocol implementations. KMyMoney now requires a <a href="https://www.aquamaniac.de/rdm/projects/gwenhywfar/files">Gwenhywfar</a>
            minimum version of 4.99.16 and an <a href="https://www.aquamaniac.de/rdm/projects/aqbanking/files">AqBanking</a>
            version of 5.99.32.</p><p><b>Important note:</b> Users who have a working setup with an older AqBanking version need to consult the
            <a href="https://www.aquamaniac.de/rdm/projects/aqbanking/wiki/AqBanking6">AqBanking Wiki for update instructions</a> before launching KMyMoney 5.0.7 to keep/update their current configuration.</p><p>Before the online functions are ready for use again, some more adjustments are necessary. Instruction are also provided on the
            <a href="https://www.aquamaniac.de/rdm/projects/aqbanking/wiki/PSD2">AqBanking Wiki</a>. These adjustments can be performed
            via the Settings/Configure AqBanking dialog accessible from within KMyMoney 5.0.7 and have to be performed per institution.</p><p>Besides this major adaption a few standing bugs have also been fixed.</p><p>Here is the list:
          <ul>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=374123">374123</a> Date Entry change to land on month part instead of day part</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=389944">389944</a> Copy SEPA credit transfer fails</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=395977">395977</a> Schedule Reports show both sides of transactions</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=400846">400846</a> Outbox can't edit a saved item</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=407072">407072</a> Scheduled Transaction Report does not show amount</li>
              <li><a href="https://bugs.kde.org/show_bug.cgi?id=410391">410391</a> Unable to change forecast days</li>
          </ul>
        </p><p>A complete description of all changes can be found in the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.7.txt">changelog</a></p>.