---
title: '        KMyMoney 4.7.1 Release Notes
      '
date: 2014-11-01 00:00:00 
layout: post
---

<p><a href="http://download.kde.org/stable/kmymoney/4.7.1/src/kmymoney-4.7.1.tar.xz.mirrorlist">KMyMoney version 4.7.1</a> is now available. This version contains fixes for several bugs, here is a short list:
          <ul>
            <li>fix ledger context menu triggering with multiple selection on Windows</li>
            <li>fix setting all amounts to zero while editing multiple transactions</li>
            <li>zero valued transactions can be entered again just like in 4.6.x</li>
            <li>fix the currency conversion dialog for really big/small conversion rates</li>
            <li>fix investment transaction unassigned value errors in some cases</li>
          </ul>
        A more complete list of changes can be found in <a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&amp;f1=cf_versionfixedin&amp;o1=equals&amp;product=kmymoney&amp;query_format=advanced&amp;resolution=FIXED&amp;v1=4.7.1">KDE's issue tracker</a>.</p><p>If you are still using 4.6.x to import your transactions and consider
        upgrading to 4.7.1 please note that transaction matching will take the
        payee into consideration. This means that payee matching rules might
        need to be setup to get matching working a expected.</p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.7.1.txt">changelog</a>. We highly recommend upgrading to 4.7.1.</p><p>The KMyMoney Development Team</p>