---
title: '        KMyMoney 4.5.96 Release Notes
      '
date: 2011-06-16 00:00:00 
layout: post
---

<p>We are happy to announce the release of KMyMoney version 4.5.96 (the second beta release on the way to KMyMoney 4.6). A few bugs were fixed since the release of the first beta and we even added some small improvements.<br /></p><p>Here is the list of the most important changes (since <a href="release-notes.php#itemKMyMoney4595ReleaseNotes">4.6 Beta 1</a>):
          <ul>
            <li>Since the OFX institution listing service run by Microsoft (TM) stopped working, we switched to the service run on <a href="http://www.ofxhome.com">www.ofxhome.com</a> which is provided on a voluntarily basis by Jesse Liesch. With his support it was easy to make the switch in KMyMoney</li>
            <li>Added Serbian Dinar as a standard currency</li>
            <li>Allow entering an interest rate with three decimal places when editing a loan</li>
            <li>Improved wording when transaction editing is canceled by selecting another transaction in the ledger</li>
            <li>Make FID an optional field during definition of an OFX account mapping</li>
            <li>Allow to base the payee name from either the PAYEEID, NAME or MEMO field in an OFX transaction</li>
          </ul>
        </p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.5.96.txt">changelog</a>.<br />
          Please try the beta (don't forget to activate the backup) and report any issues you find so 4.6 will become a rock solid release.</p><p>The KMyMoney Development Team</p>