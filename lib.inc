<?php

function bugList($type, $_version="",$options="")
{
	if (strpos($_version, '&') !== false) {
		list($version, $branch) = explode('&', $_version);
	} else {
		$version = $_version;
		$branch = '';
	}

	$baseColumns = "&columnlist=component%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Creporter_realname%2Cassigned_to_realname%2Cbug_severity%2Cversion";
	$columns = array(
		"open" => $baseColumns,
		"resolved" => "&columnlist=component%2Cbug_severity%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Creporter_realname%2Ccf_versionfixedin",
		"assigned" => $baseColumns."%2Ctarget_milestone",
		"todo" => $baseColumns."%2Ctarget_milestone",
		"flags" => $baseColumns."%2Ccf_versionfixedin%2Cflagtypes.name",
	);

	$status = array(
		"resolved" => "&bug_status=RESOLVED&resolution=FIXED&resolution=DUPLICATE&resolution=WORKSFORME&resolution=MOVED&resolution=UPSTREAM&resolution=DOWNSTREAM",
		"open" => "&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=NEEDSINFO",
		"assigned" => "&bug_status=ASSIGNED",
		"todo" => "&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=REOPENED",
		"fixed" => "&bug_status=RESOLVED&resolution=FIXED",
	);

	$url = "https://bugs.kde.org/buglist.cgi?product=kmymoney"
			."&query_format=advanced"
			;

	$types = array(
		"all" => "",
		"changelog" => "",
		"wishlist" => "&bug_severity=wishlist",
		"crash" => "&bug_severity=crash",
		"critical" => "&bug_severity=critical",
		"grave" => "&bug_severity=grave",
		"major" => "&bug_severity=major",
		"normal" => "&bug_severity=normal",
		"minor" => "&bug_severity=minor",
		"juniorjobs" => "&keywords=junior-jobs&keywords_type=allwords",
		"allbugs" => "&bug_severity=crash&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=grave&bug_severity=critical",
	);

	$versions = array(
		"4.3" => "&o2=regexp&v2=4.3|4.3.[0-7]|4.2.[89][0-9]",
		"4.4" => "&o2=regexp&v2=4.4|4.4.[0-7]|4.3.[89][0-9]",
		"4.5" => "&o2=regexp&v2=4.5|4.5.[0-7]|4.4.[89][0-9]",
		"4.6" => "&o2=regexp&v2=4.6|4.6.[0-7]|4.5.[89][0-9]",
		"4.7" => "&o2=regexp&v2=4.7|4.7.[0-7]|4.6.[89][0-9]",
		"4.7.0" => "&o2=regexp&v2=4.7.0",
		"4.7.1" => "&o2=regexp&v2=4.7.1",
		"4.7.2" => "&o2=regexp&v2=4.7.2",
		"4.8" => "&o2=regexp&v2=4.8|4.8.[0-7]|4.7.[89][0-9]",
		"4.8.0" => "&o2=regexp&v2=4.8.0",
		"4.8.1" => "&o2=regexp&v2=4.8.1",
		"4.8.2" => "&o2=regexp&v2=4.8.2",
		"4.8.3" => "&o2=regexp&v2=4.8.3",
		"4.8.4" => "&o2=regexp&v2=4.8.4",
		"4.8.5" => "&o2=regexp&v2=4.8.5",
		"4.8.6" => "&o2=regexp&v2=4.8.6",
		"4.8.7" => "&o2=regexp&v2=4.8.7",
		"4.8.8" => "&o2=regexp&v2=4.8.8",
		"4.8.9" => "&o2=regexp&v2=4.8.9",
		"5.0" => "&o2=regexp&v2=5.0|5.0.[0-7]|4.8.[89][0-9]",
		"5.0.0" => "&o2=regexp&v2=5.0.0",
		"5.0.1" => "&o2=regexp&v2=5.0.1",
		"5.0.2" => "&o2=regexp&v2=5.0.2",
		"5.0.3" => "&o2=regexp&v2=5.0.3",
		"5.0.4" => "&o2=regexp&v2=5.0.4",
		"5.0.5" => "&o2=regexp&v2=5.0.5",
		"5.0.6" => "&o2=regexp&v2=5.0.6",
		"5.0.7" => "&o2=regexp&v2=5.0.7",
		"5.0.8" => "&o2=regexp&v2=5.0.8",
		"5.0.9" => "&o2=regexp&v2=5.0.9",
	);

	$search = array(
		"open" => "&f2=version".$versions[$version],
		"fixed" => "&f2=cf_versionfixedin".$versions[$version],
		"assigned" => "&f2=target_milestone".$versions[$version],
		"todo" => "&f2=target_milestone".$versions[$version],
		"notin5" => "&f2=cf_versionfixedin&f3=cf_versionfixedin&f4=flagtypes.name&o2=regexp&o3=notsubstring&o4=notsubstring&v2=4.8.%5B12345%5D&v3=5.0&v4=Version_5-"
	);

	$searchBranches = "&f4=longdesc&o4=substring&v4=$branch";

	$orders = array(
		"status" => "&order=bug_status%2Cpriority%2Cassigned_to%2Cbug_id",
		"severity" => "&order=bug_severity%2Cbug_status%2Cpriority%2Cassigned_to%2Cbug_id",
		"versionfixed" => "&order=cf_versionfixedin",
	);
	$urls = array(
		"all" => $url.$types['all'].$columns['open'].$orders['status'],
		"allbugs" => $url.$types['allbugs'].$columns['open'].$orders['status'],
		"wishlist" => $url.$types['wishlist'].$status['open'].$columns['open'].$orders['status'],
		"crash" => $url.$types['crash'].$status['open'].$columns['open'].$orders['status'],
		"major" => $url.$types['major'].$status['open'].$columns['open'].$orders['status'],
		"normal" => $url.$types['normal'].$status['open'].$columns['open'].$orders['status'],
		"minor" => $url.$types['minor'].$status['open'].$columns['open'].$orders['status'],
		"juniorjobs" => $url.$types['juniorjobs'].$status['open'].$columns['open'].$orders['status'],
		"changelog" => $url.$types['changelog'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['versionfixed'],
		"openbugs" => $url.$types['allbugs'].$status['open'].$columns['open'].$search['open'].$orders['status'],
		"resolvedbugs" => $url.$types['allbugs'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedcrashbugs" => $url.$types['crash'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedmajorbugs" => $url.$types['major'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvednormalbugs" => $url.$types['normal'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedminorbugs" => $url.$types['minor'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"resolvedfeatures" => $url.$types['wishlist'].$status['resolved'].$columns['resolved'].$search['fixed'].$orders['status'],
		"assignedfeatures" => $url.$types['wishlist'].$status['assigned'].$columns['assigned'].$search['assigned'].$orders['status'],
		"todofeatures" => $url.$types['wishlist'].$status['todo'].$columns['todo'].$search['todo'].$orders['status'],
		"notin5" => $url.$status['fixed'].$columns['flags'].$search['notin5'].$orders['status'],
	);

	$url = "";
	if (isset($urls[$type])) {
		$url = $urls[$type];
		if ($branch != '')
			$url .= $searchBranches;
		if ($options)
			$url .= "&". $options;
	}
	return $url;
}
