---
layout: page
title: Team
css-include: /css/main.css
---

# The KMyMoney development team

## Who we are and what we do

Our core development team is quite small, but KMyMoney was developed by many people.

{% for dev in site.data.team %}
  {% if dev.active == true %}
  <hr class="mb-4" />
  <div class="d-flex" itemscope itemtype="http://schema.org/Person">
    <div class="flex-fill mb-2">
      <h3 class="mt-0" itemprop="name">{{ dev.firstname }} {{ dev.name }}</h3>
      <strong>Location:</strong> <spam itemprop="address">{{ dev.location }}</spam><br />
      {% if dev.homepage %}
        <strong>Homepage:</strong> <a itemprop="url" href="{{dev.homepage}}">{{dev.homepage}}</a><br />
      {% endif %}
      <strong>Roles:</strong> {{ dev.roles }}<br />
      <strong>Since:</strong> {{ dev.since }}<br /><br />
      <strong>About {{ dev.firstname }} </strong>
      {{dev.description | markdownify}}
    </div>
    <div class="kTeamMember">
      <img itemprop="image" src="{{ dev.image }}" alt="Photo of {{ dev.firstname }} {{ dev.name }}" />
    </div>
  </div>
  {% endif %}
{% endfor %}

<br class="mb-5" />

# Inactive Developers

{% for dev in site.data.team %}
  {% if dev.active != true %}
  <hr class="mb-4" />
  <div class="d-flex" itemscope itemtype="http://schema.org/Person">
    <div class="flex-fill mb-2">
      <h3 class="mt-0" itemprop="name">{{ dev.firstname }} {{ dev.name }}</h3>
      <strong>Location:</strong> <spam itemprop="address">{{ dev.location }}</spam><br />
      {% if dev.homepage %}
        <strong>Homepage:</strong> <a itemprop="url" href="{{dev.homepage}}">{{dev.homepage}}</a><br />
      {% endif %}
      <strong>Roles:</strong> {{ dev.roles }}<br />
      <strong>Since:</strong> {{ dev.since }}<br /><br />
      <strong>About {{ dev.firstname }} </strong>
      {{dev.description | markdownify}}
    </div>
    <div class="kTeamMember">
      <img itemprop="image" src="{{ dev.image }}" alt="Photo of {{ dev.firstname }} {{ dev.name }}" />
    </div>
  </div>
  {% endif %}
{% endfor %}
