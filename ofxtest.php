<?php

/*
 * Copyright 2017,2019 Ralf Habacker <ralf.habacker@freenet.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*
 * ofx test web service for kmymoney login test
 *
 * based on http://www.ofx.net/downloads/OFX%202.2.pdf
 */

# show errors
$debug = isset($_REQUEST['debug']);
if ($debug) {
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
}

$hasOffsetSupport = version_compare(phpversion(), '5.5.10', '>=');
/*
  use namespace for validating response against xsd file https://www.ofx.net/downloads/OFX%202.2.0%20schema.zip

  wget --post-file=ofxrequest.xml -O response.xml "https://kmymoney.org/ofxtest.php?debug=1&useNameSpace=1"
  xmllint --noout --schema "OFX 2.2.0 schema\OFX2_Protocol.xsd" response.xml
*/
$useOFXNameSpace = isset($_REQUEST['useNameSpace']);

/**
 * convert an ofx date string into a timestamp
 */
function strToDate($s)
{
	global $debug;

	$i = strpos($s, "[");
	if ($i !== false) {
		$dt = substr($s, 0, $i);
		$tzs = substr($s, $i+1, strlen($s)-$i-2);
		try {
			if (is_numeric($tzs[0]))
				$tz = new DateTimeZone('+'.$tzs);
			else
				$tz = new DateTimeZone($tzs);
		} catch (Exception $e) {
			echo '<!-- could not create time zone: '.$e->getMessage()."-->";
			error_log("'$s' could not create time zone: ".$e->getMessage());
			return 0;
		}
	} else {
		$dt = $s;
		$tz = new DateTimeZone("GMT");
	}
	if (strlen($dt) == 8)
		$d = DateTime::createFromFormat("YmdHis", $dt."000000", $tz);
	elseif (strlen($dt) == 14)
		$d = DateTime::createFromFormat("YmdHis", $dt, $tz);
	elseif (strlen($dt) == 18)
		$d = DateTime::createFromFormat("YmdHis#u", $dt, $tz);
	else
		$d = 0;
	if (!$d) {
		error_log("'$s' invalid date format");
		return 0;
	}
	$t = $d->getTimestamp();
	if ($debug)
        error_log("$dt '".$tz->getName()."' -> $t");
	return $t;
}

function _checkDate($mode, $s="")
{
    if ($mode == 0) {
        echo "<table>\n"
        ."<tr><th>ofx date</th><th>timestamp</th><th>formatted output</th></tr>\n";
        return;
    }
    elseif ($mode == 1) {
        $d = strToDate($s);
        $date = new DateTime("@$d");
        $tzs = "CET";
        $date->setTimezone(new DateTimeZone($tzs));
        $s2 = $date->format("Y-m-d H:i:s.u");
        //$s2 = date("Y-m-d H:i:s.u", $d);
        echo "<tr><td>$s</td><td>$d</td><td>$s2 [$tzs]</td></tr>\n";
	}
	elseif ($mode == 2)
        echo "</table>";
}

if (isset($_REQUEST['debug']) && $_REQUEST['debug'] == 'date') {
	$data = array(
		"20181201",
		"20181201000000",
		"20181201000000.000",
		"20181201000000[GMT]",
		"20181201000000[EST]",
		"20181201000000[MAGT]",
		"20181201000000[SST]",
		"20181201000000[12]",
		"20181201000000[-11]",
		"20181201000000[+12]",
		"20181201000000[12.00]",
		"20181201000000[-11.50]",
		"20181201000000[+10.50]",
		"20181201000000[+12:MAGT]",
		"20181201000000[12:MAGT]",
		"20181201000000[-11:SST]",
		# from spec section 3.2.8.2 Date and Datetime
		"19961005132200.124[-5:EST]",
	);

	_checkDate(0);
	foreach($data as $s) {
        _checkDate(1, $s);
	}
	_checkDate(2);
	exit(1);
}

/**
 * check that datetime string $a is in range between $start and $end
 */
function isInRange($a, $start, $end)
{
	global $hasOffsetSupport;
	global $debug;

	if ($hasOffsetSupport) {
		$t = strToDate($a);
		$r = (empty($start) || $t >= strToDate($start)) && (empty($end) || $t <= strToDate($end));
	} else {
		$r = true;
	}
	if ($debug)
		error_log("isInRange($a, $start, $end) -> $r");
	return $r;
}

$today = date('Ymd');

$config = array(
	'OFXHEADER' => '200',
	'VERSION' => '220',
	'USERID' => 'test',
	'USERPASS' => 'test1',
	'ORG' => 'test',
	'FID' => 'test',
	'DESC' => 'Power Checking',
	'PHONE' => '0088224482',
	'BANKID' => '123456789',
	'accounts' => array(
		'00001234' => array(
			'ACCTID' => '00001234',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account with check and atm statements',
			'BANKTRANLIST' => array(
				'20171001' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => $today,
					'TRNAMT' => -200.00,
					'FITID' => '00002',
					'NAME' => 'Test1',
					'CHECKNUM' => '1000'
				),
				'20171002' => array(
					'TRNTYPE' => 'ATM',
					'DTPOSTED' => $today,
					'DTUSER' => '20051020',
					'TRNAMT' => -300.00,
					'FITID' => '00003',
					'NAME' => 'Test2',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '200.29',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '200.29',
				'DTASOF' => $today,
			),
		),
		'00001235' => array(
			'ACCTID' => '00001235',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'another account with check statement',
			'BANKTRANLIST' => array(
				'20171001' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => $today,
					'TRNAMT' => 200.00,
					'FITID' => '00001',
					'NAME' => 'Test3',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '100.29',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '100.29',
				'DTASOF' => $today,
			),
		),
		'00001236' => array(
			'ACCTID' => '00001236',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account for testing dates with different time zones',
			'BANKTRANLIST' => array(
				'1' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201',
					'TRNAMT' => -140.00,
					'FITID' => '0001',
					'NAME' => 'Landlord-1',
					'MEMO' => 'short date - 12:00 AM (the start of the day), GMT',
				),
				'2' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000',
					'TRNAMT' => -140.00,
					'FITID' => '0002',
					'NAME' => 'Landlord-2',
					'MEMO' => 'date with time without tz',
				),
				'3' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000.000',
					'TRNAMT' => -140.00,
					'FITID' => '0003',
					'NAME' => 'Landlord-3',
					'MEMO' => 'date with time and msecs without tz',
				),
				'4' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[GMT]',
					'TRNAMT' => -140.00,
					'FITID' => '0004',
					'NAME' => 'Landlord-4',
					'MEMO' => 'date with time and default tz',
				),
				'5' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000.000[GMT]',
					'TRNAMT' => -140.00,
					'FITID' => '0005',
					'NAME' => 'Landlord-5',
					'MEMO' => 'date with time, msecs and default tz',
				),
				'6a' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[-11]',
					'TRNAMT' => -140.00,
					'FITID' => '0006a',
					'NAME' => 'Landlord-6a',
					'MEMO' => 'date with time, tz offset without tz identifier (-11)',
				),
				'6b' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[-11:SST]',
					'TRNAMT' => -140.00,
					'FITID' => '0006b',
					'NAME' => 'Landlord-6b',
					'MEMO' => 'date with time, tz offset and identifier (-11:SST)',
				),
				'7a' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[12]',
					'TRNAMT' => -140.00,
					'FITID' => '0007a',
					'NAME' => 'Landlord-7a',
					'MEMO' => 'date with time, tz offset without tz identifier (12)',
				),
				'7b' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[12:MAGT]',
					'TRNAMT' => -140.00,
					'FITID' => '0007b',
					'NAME' => 'Landlord-7b',
					'MEMO' => 'date with time, tz offset and identifier (12:MAGT)',
				),
				'8a' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[+12]',
					'TRNAMT' => -140.00,
					'FITID' => '0008a',
					'NAME' => 'Landlord-8a',
					'MEMO' => 'date with time, tz offset without tz identifier (+12)',
				),
				'8b' => array(
					'TRNTYPE' => 'XFER',
					'DTPOSTED' => '20181201000000[+12:MAGT]',
					'TRNAMT' => -140.00,
					'FITID' => '0008b',
					'NAME' => 'Landlord-8b',
					'MEMO' => 'date with time, tz offset and identifier (+12:MAGT)',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-980',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-980',
				'DTASOF' => $today,
			),
		),
		'00001237' => array(
			'ACCTID' => '00001237',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account with several check statements',
			'BANKTRANLIST' => array(
				'20171001' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00001',
					'CHECKNUM' => '123',
					'NAME' => 'Test1',
				),
				'20171002' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171002',
					'TRNAMT' => -123.00,
					'FITID' => '00002',
					'CHECKNUM' => '000123',
					'NAME' => 'Test2',
				),
				'20171003' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171003',
					'TRNAMT' => -124.00,
					'FITID' => '00003',
					'CHECKNUM' => '124',
					'NAME' => 'Test3',
				),
				'20171004' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171004',
					'TRNAMT' => -125.00,
					'FITID' => '00004',
					'CHECKNUM' => '125',
					'NAME' => 'Test4',
				),
				'20171005' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171005',
					'TRNAMT' => -126.00,
					'FITID' => '00005',
					'CHECKNUM' => '126',
					'NAME' => 'Test5',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-621.29',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-621.29',
				'DTASOF' => $today,
			),
		),
		'bug290051' => array(
			'ACCTID' => 'bug290051',
			'ACCTTYPE' => 'CHECKING',
			# non standard field
			'MEMO' => 'account for testing bug 290051 (it contains 4 checks with different check numbers, but same amount',
			'BANKTRANLIST' => array(
				'1' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00001',
					'CHECKNUM' => '1',
					'NAME' => 'Test1',
				),
				'2' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00002',
					'CHECKNUM' => '2',
					'NAME' => 'Test2',
				),
				'3' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00003',
					'CHECKNUM' => '3',
					'NAME' => 'Test3',
				),
				'4' => array(
					'TRNTYPE' => 'CHECK',
					'DTPOSTED' => '20171001',
					'TRNAMT' => -123.00,
					'FITID' => '00004',
					'CHECKNUM' => '4',
					'NAME' => 'Test4',
				),
			),
			'LEDGERBAL' => array(
				'BALAMT' => '-492,00',
				'DTASOF' => $today,
			),
			'AVAILBAL' => array(
				'BALAMT' => '-492,00',
				'DTASOF' => $today,
			),
		),
	),
	'CURDEF' => 'USD'
);

function getStatusCode(&$data)
{
	global $config;

	$code = 0;
	if ($data['USERID'] != $config['USERID'] || $data['USERPASS'] != $config['USERPASS'])
		$code = 15500;
	return $code;
}

function getStatusResponse($code)
{
	if ($code != 0)
		return '<STATUS>'
		.'<CODE>'.$code.'</CODE>'
		.'<SEVERITY>ERROR</SEVERITY>'
		.'</STATUS>'
		;

	return '<STATUS>'
	.'<CODE>0</CODE>'
	.'<SEVERITY>INFO</SEVERITY>'
	.'</STATUS>'
	;
}

function accountInformationRequest(&$data)
{
	global $config;

	$code = getStatusCode($data);
	$s = '<ACCTINFOTRNRS>'
	.'<TRNUID>'.$data['TRNUID'].'</TRNUID>'
	.getStatusResponse($code)
	;
	if (!$code) {
		$s .= '<ACCTINFORS>'
		.'<DTACCTUP>'.$data['DTACCTUP'].'</DTACCTUP>'
		;
		foreach ($config['accounts'] as $account) {
			$s .= '<ACCTINFO>'
			.'<DESC>'.$config['DESC'].'</DESC>'
			.'<PHONE>'.$config['PHONE'].'</PHONE>'
			.'<BANKACCTINFO>'
			.'<BANKACCTFROM>'
			.'<BANKID>'.$config['BANKID'].'</BANKID> '
			.'<ACCTID>'.$account['ACCTID'].'</ACCTID>'
			.'<ACCTTYPE>'.$account['ACCTTYPE'].'</ACCTTYPE>'
			.'</BANKACCTFROM>'
			.'<SUPTXDL>Y</SUPTXDL>'
			.'<XFERSRC>Y</XFERSRC>'
			.'<XFERDEST>Y</XFERDEST>'
			.'<SVCSTATUS>ACTIVE</SVCSTATUS>'
			.'</BANKACCTINFO>'
			.'</ACCTINFO>'
			;
		}
		$s .= '</ACCTINFORS>'
		;
	}
	$s .= '</ACCTINFOTRNRS>'
	;
	return $s;
}

function statementDownloadRequest(&$data)
{
	global $config, $today;

	$code = getStatusCode($data);
	$s = '<SIGNONMSGSRSV1>'
	.'<SONRS>'
	.getStatusResponse($code)
	.'<DTSERVER>20051029101003</DTSERVER>'
	.'<LANGUAGE>ENG</LANGUAGE>'
	.'<DTPROFUP>19991029101003</DTPROFUP>'
	//.'<DTACCTUP>'.$data['DTACCTUP'].'</DTACCTUP>'
	.'<FI>'
	.'<ORG>'.$data['ORG'].'</ORG>'
	.'<FID>'.$data['FID'].'</FID>'
	.'</FI>'
	.'</SONRS>'
	.'</SIGNONMSGSRSV1>'
	;
	if ($code)
		return $s;

	$s .= '<BANKMSGSRSV1>'
	.'<STMTTRNRS>'
	.'<TRNUID>'.$data['TRNUID'].'</TRNUID>'
	;

	if (!isset($config['accounts'][$data['ACCTID']])) {
		$code = 2003; // account not found
	}
	$s .= getStatusResponse($code);
	// account has been found
	if (!$code) {
		$account = &$config['accounts'][$data['ACCTID']];
		$dtstart = $data['DTSTART'];
		$dtend = isset($data['DTEND']) ? $data['DTEND'] : $today;
		$s .= '<STMTRS>'
		.'<CURDEF>'.$config['CURDEF'].'</CURDEF>'
		.'<BANKACCTFROM>'
		.'<BANKID>'.$config['BANKID'].'</BANKID> '
		.'<ACCTID>'.$account['ACCTID'].'</ACCTID>'
		.'<ACCTTYPE>'.$account['ACCTTYPE'].'</ACCTTYPE>'
		.'</BANKACCTFROM>'
		.''
		.'<BANKTRANLIST>'
		.'<DTSTART>'.$dtstart.'</DTSTART>'
		.'<DTEND>'.$dtend.'</DTEND>'
		;
		// add transactions
		foreach ($account['BANKTRANLIST'] as $t) {
			if (!isInRange($t['DTPOSTED'], $dtstart, $dtend))
				continue;
			$s .= '<STMTTRN>';
			foreach ($t as $key => $value) {
				$s .= "<$key>$value</$key>";
			}
			$s .= '</STMTTRN>'
			;
		}
		$s .= '</BANKTRANLIST>'
		.'<LEDGERBAL>'
		.'<BALAMT>'.$account['LEDGERBAL']['BALAMT'].'</BALAMT>'
		.'<DTASOF>'.$account['LEDGERBAL']['DTASOF'].'</DTASOF>'
		.'</LEDGERBAL>'
		.''
		.'<AVAILBAL>'
		.'<BALAMT>'.$account['AVAILBAL']['BALAMT'].'</BALAMT>'
		.'<DTASOF>'.$account['AVAILBAL']['DTASOF'].'</DTASOF>'
		.'</AVAILBAL>'
		.'</STMTRS>';
	}
	$s .= '</STMTTRNRS>'
	.'</BANKMSGSRSV1>'
	;
	return $s;
}

$validOFXRecord = false;
$rawPostData = file_get_contents("php://input");
$postData = explode("\r\n", $rawPostData);
if ($debug)
	error_log(print_r($postData,true));

// parse input
foreach($postData as $line) {
	$l = trim($line);
        if ($debug)
                error_log($l);
	if (strcmp($l, "<OFX>") === 0) {
		$validOFXRecord = true;
	}
	if (0 === strpos($l, '<')) {
		$a = explode(">", $l);
		$b = explode("<", $a[0]);
		$key = $b[1];
		$value = $a[1];
		$data[$key] = $value;
		if ($debug)
			error_log("$key $value");
	}
}

if ($validOFXRecord) {
	header("Content-Type: application/x-ofx");
	$code = getStatusCode($data);
	$s = '<?xml version="1.0"?>';
	if ($debug)
		$s .= '<!-- php version: '.phpversion() .' -->';
	$s .= '<?OFX OFXHEADER="'.$config['OFXHEADER'].'" VERSION="'.$config['VERSION'].'" SECURITY="NONE" OLDFILEUID="NONE" "NEWFILEUID="NONE"?>'
       . ($useOFXNameSpace ? '<ofx:OFX xmlns:ofx="http://ofx.net/types/2003/04">' : '<OFX>');
	if ($code) {
		$s .= getStatusResponse($code);
	} else if (isset($data['ACCTINFOTRNRQ'])) {
		$s .= accountInformationRequest($data);
	} else if (isset($data['BANKMSGSRQV1'])) {
		$s .= statementDownloadRequest($data);
	} else
		$s .= getStatusResponse(2028);
	$s .= ($useOFXNameSpace ? '</ofx:OFX>' : '</OFX>');
	echo $s;
	if ($debug)
		error_log($s);
} else {
	header("Content-Type: text/html");
	echo "<pre>"
	."This service is intended for access from KMyMoney only\n\n"
	."It supports assigning an ofx online account to a KMyMoney account\n"
	."and updating the account from the online account.\n\n"
	."---------------------- access data ----------------------\n"
	."Choose adding manual account with the following settings:\n\n"
	."Org: ".$config['ORG']."\n"
	."Fid: ".$config['FID']."\n"
	."Url: "."http".(($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://").$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."\n"
	."User: ".$config['USERID']."\n"
	."Password: ".$config['USERPASS']."\n"
	."\n"
	."---------------------- bank data ----------------------\n"
	."bank identification:".$config['BANKID']."\n"
	."number of accounts: ".sizeof($config['accounts'])."\n"
	."\n"
	."---------------------- accounts ----------------------\n"
	." account  \t type   \t notes\n";
	foreach($config['accounts'] as $account) {
		echo $account['ACCTID']."\t". $account['ACCTTYPE']."\t". $account['MEMO']."\n";
	}
	echo "\n"
	."---------------------- limitations ----------------------\n";
	if (!$hasOffsetSupport)
		echo "- The limitation of statements by date does not work, caused by the PHP version used\n";
	echo "</pre>";
}
?>
