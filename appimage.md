---
title: "the BEST Personal Finance Manager for FREE Users, full stop."
layout: page
---

# How to install and use the AppImage version?

For Linux users, who want to use a newer version than is available in their distro repository, we offer an AppImage version which reflects the current stable version including the latest fixes. It is build on a daily basis straight from the source.

Here's how you install and run it:

* Download the file from [https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/](https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/) to any location you like.

* Open a terminal window (e.g. using Konsole). The following instructions are
  to be executed in this terminal window

* Change into the directory where you downloaded the file in the first step above.

* Make the file executable by entering the command `chmod +x name-of-downloaded-file`
  and pressing the Return key.

* Execute that file with by entering `./name-of-downloaded-file` and pressing the
  Return key. The dot and the slash are important.

For the very adventurous Linux user, there is an AppImage with the latest development
version at [https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/](https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/).
