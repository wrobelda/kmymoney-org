<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of resolved features
#
# syntax:
#   resolvedfeatures.php           - shows all resolved features
#   resolvedfeatures.php?<version> - shows resolved features of <version> e.g. 4.8.1
#                                    or 4.8 for all releases of the mentioned branch
#
	include(dirname(__FILE__)."/lib.inc");
	$version = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglist('resolvedfeatures',$version);
	Header("Location: $url");
?>