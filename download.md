---
layout: page
title: Download
sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        <p>
        KMyMoney is already available on majority of Linux distributions. You
        can install it from the KDE Software Center.
        </p>
  - name: AppImage
    icon: /assets/img/appimage.svg
    description: >
        <p>
          For Linux users, who want to use a newer version than is available
          in their distro repository, we offer an AppImage
          (<a href="https://appimage.org/">learn about AppImages</a>)
          version which reflects
          the current stable version including the latest fixes. It is build on a
          daily basis straight from the source.
        </p>
        <p>
          Installation instructions can be found on our <a href="/appimage.html">AppImage page</a>.
        </p>
  - name: Windows
    icon: /assets/img/windows.svg
    description: >
        KMyMoney is available for Windows. An `.exe` can be found on our
        [download server](https://download.kde.org/stable/kmymoney/5.0.6/).
        This is a cross compiled and includes KBanking, Gpg2 and translations.

        We also provide daily generated preview builds:
        [Windows MSVC 64bit](https://binary-factory.kde.org/job/KMyMoney_Nightly_win64/)
  - name: macOS
    icon: /assets/img/macOS.svg
    description: >
        Daily built [stable version](https://binary-factory.kde.org/job/KMyMoney_Release_macos//)
        of KMyMoney is available for macOS. An old version is also available
        in [MacPorts](https://www.macports.org/ports.php?by=name&substr=kmymoney).
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        You can find KMyMoney latest stable release
        <a href="https://download.kde.org/stable/kmymoney/">here</a> and
        installation instructions in the
        <a href="https://techbase.kde.org/Projects/KMyMoney#Installation">
        KDE TechBase wiki</a>.
  - name: Git
    icon: /assets/img/git.svg
    description: >
        The KMyMoney Git repository can be found in the
        <a href="https://invent.kde.org/office/kmymoney">KDE GitLab</a>.

        To clone KMyMoney use:

        <code>git clone https://invent.kde.org/office/kmymoney.git</code>
---

<h1>Download</h1>

<table class="distribution-table">
{% for source in page.sources %}
    <tr class="title-row">
        <td rowspan="2" width="100">
            <img src="{{ source.icon }}" alt="{{ source.name }}">
        </td>
        <th>{{ source.name }}</th>
    </tr>
    <tr>
        <td>{{ source.description | markdownify }}</td>
    </tr>
{% endfor %}
</table>
