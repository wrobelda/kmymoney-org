---
title: Screenshots
layout: default
css-include: /css/main.css
---

<main class="container">
  <h1>Screenshots</h1>

  {% for image in site.data.screenshots %}
    <div class="block pt-4 pb-4">
      <div class="kAppInfo">
        <div class="kAppInfo-content">
          <h2>{{ image.title }}</h2>
          <p style="max-width:900px;">
            {{image.description | markdownify}}
          </p>
        </div>
        {% if image.video %}
          <video src="{{ image.large }}" autoplay loop muted />
        {% else %}
          <a href="{{ image.large }}" data-toggle="lightbox"><img alt="{{ image.tip }}" src="{{ image.large }}" class="preview img-fluid" /></a>
        {% endif %}
      </div>
    </div>
  {% endfor %}
</main>
