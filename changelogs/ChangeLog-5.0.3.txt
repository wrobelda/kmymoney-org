commit 6076cd210c87fa009ed26d410b1731b22a76182a
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Jan 27 13:25:20 2019 +0100

    Bumped version to 5.0.3

commit c9bacc5dc70ebdbb046a80331efe6f7ada90484f
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 26 15:49:03 2019 +0100

    Avoid destruction of object that is still being used
    
    The m_qifReader object was deleted to early.
    
    BUG: 403565
    FIXED-IN: 5.0.3

commit 6f793d892d70b3934d15c876c7e741b2567694e4
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 26 15:10:54 2019 +0100

    Update last used directory when opening a file
    
    BUG: 403608
    FIXED-IN: 5.0.3

commit e01691a08631508bff4ad648b274caa8acd2cf00
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 26 14:40:27 2019 +0100

    Updated currency entry for Angolan Kwanza
    
    BUG: 403617
    FIXED-IN: 5.0.3

commit 139aa5ea4c15e43cafae7bb924f3aaf1d3fa7b7b
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 26 14:20:30 2019 +0100

    Support sell w/o asset account
    
    Summary:
    Allow a sell operation that reduces the number of shares to pay fees.
    This is to handle mutual fund account fees. There is no money
    transferred -- only a reduction in shares.
    
    BUG: 398133
    
    Test Plan: I have tested entry and retrieval of the transaction. What needs to be tested how this change affects reporting. I can't do that.
    
    Reviewers: ostroffjh
    
    Differential Revision: https://phabricator.kde.org/D15215

commit b1fdee150af99db3134689efcb1ca3f5aff7daaa
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 26 08:15:24 2019 +0100

    Fix check number handling
    
    KMyMoney versions prior to 5.0 allowed to re-use check numbers. This is
    not possible with the current 5.0 versions. This change will bring back
    the original functionality.
    
    In case KMyMoney detects a duplicate check number it informs the user.
    The user then has the possibility to assign the next free number or
    leave the number as entered. It will not be cleared in this case
    anymore.
    
    BUG: 402101
    FIXED-IN: 5.0.3

commit 91bc1a7adf09a856ee3070a2aadf147e3066bd62
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Jan 24 08:28:47 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours
    
    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit 1abdb472bfea9bd365c95c6cc102b98e1f1eaf6f
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Jan 24 07:14:29 2019 +0100

    GIT_SILENT made messages (after extraction)

commit 6b60d7012ab163b83520cd3163d60083e12f8ea6
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Jan 23 15:54:26 2019 +0100

    Add possible selected tag when leaving the transaction editor
    
    BUG: 403529
    FIXED-IN: 5.0.3

commit 2e5b3e53e6eff28c50269f7780b4dd80b7c6a469
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jan 21 20:10:29 2019 +0100

    Resolved unused variable warnings

commit 462e70c278ec7b2f5711bfdab3387c6b48fb52b8
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jan 21 17:28:24 2019 +0100

    Fix testcase
    
    The JPY in this testcase does not have a fractional part, so we cannot
    expect it in the reports output.

commit f69ee7d76454a7e57fb998057ce16062c17702e4
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jan 21 16:34:32 2019 +0100

    Fix plugin loader
    
    The Qt plugin system searches and reports plugins in the following
    order:
    
    - current directory
    - paths provided in QT_PLUGIN_PATH (separated by QDir::listSeparator)
    - path provided by QLibraryInfo::location(QLibraryInfo::PluginsPath)
    
    The KMyMoney plugin loader stores all plugin metadata of the returned
    plugins in a QMap to process them. It was not checked if a plugin was
    already found and so the last instance always won the game leaving the
    ones in the current directory or in one of the directories mentioned in
    QT_PLUGIN_PATH without effect.
    
    This change makes sure to load and use the first plugin found and also
    prints some more information on the console which plugins were found and
    which ones are loaded.

commit ca736511839941dedf37f0636651deb9bc4ae923
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Jan 21 16:22:42 2019 +0100

    Remove debug output

commit 447213e04d6e7ab9022caeb5c258800625036967
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Jan 20 13:48:31 2019 +0100

    Improve transaction importer
    
    The transaction importer - used during online banking statement download
    or file import - used to either copy the categorization of a transaction
    that exactly matched the value of the imported transaction or the last
    transaction found for that payee in the same account.
    
    This is a bit crazy if you have two alternating and slightly varying
    payments from/to the same payee with very different amouts. This leads
    to the fact, that in most cases it will choose the last one (which is
    the wrong one in this scenario).
    
    With this change the importer now looks at all historical transactions
    for the payee in the account and if no exact match is found use the
    transaction with the smallest difference in value to the imported
    transaction. This improves the above scenario very much.

commit ccda8ef56380525989670612be6f642665aa7026
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Jan 20 07:33:31 2019 +0100

    GIT_SILENT made messages (after extraction)

commit bd103f87388b56c873210266887272d490b22ad2
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 19 16:32:16 2019 +0100

    Setup the transaction commodity based on the trading currency
    
    For new investment transactions the commodity was always set to the
    baseCurrency instead of the securities trading currency. This usually
    caused the values to be rounded to two decimals (smallest account
    fraction of the base currency).
    
    This change fixes the problem by not looking at the empty original
    transaction in this case.
    
    BUG: 403249
    FIXED-IN: 5.0.3

commit e38492cbc059796addb5a62861e75b851b2428e0
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Jan 19 16:27:52 2019 +0100

    Fix security creation logic
    
    In case no smallest account fraction is provided in the ctor of a
    MyMoneySecurity object it should use the same value as provided for the
    smallest cash fraction. Providing a default value of 100 destroys this
    logic. This forced Palladium to have values always rounded to two
    decimals.
    
    In case the user has such a security on file there is no other way as
    manually fixing it by copying the value from the SCF attribute to the
    SAF attribute in the XML file and the value in the smallestCashFraction
    column to the smallestAccountFraction column in the database.
    
    CCBUG: 403249

commit 26621fb9d822af89ffeba9756c7f2a8264d6bf21
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Fri Jan 18 13:24:21 2019 +0100

    Update copyright info

commit 17c4af4a7eb1582e98979fc20acca4bc0da17658
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jan 17 09:17:50 2019 +0100

    Display name of equity with strikeout font if the equity account has been closed.
    
    Summary: Display name of equity with strikeout font if the equity account has been closed.
    
    Test Plan:
    Open Investment view.
    Verify closed equity accounts are displayed with strike through font and open equity accounts are displayed with normal font.
    Close an equity account.
    Verify the display of the account just closed changed from normal to striked through font.
    Reopen the equity account.
    Verify the display of the account just reopened changed from striked through to normal font.
    
    Reviewers: #kmymoney, tbaumgart
    
    Reviewed By: #kmymoney, tbaumgart
    
    Subscribers: #kmymoney
    
    Tags: #kmymoney
    
    Differential Revision: https://phabricator.kde.org/D16433
    
    (cherry picked from commit b17d61f598bb9bf240db41e3136606bb54424c04)

commit 741e848593162aa670c735b812b0574dbb483907
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jan 17 08:17:57 2019 +0100

    Fix division by zero errors in QueryTable
    
    Summary:
    Code used to try to divide by zero. Fixed it to always check for the
    denominator being 0 beforehand.
    
    Original patch by Zoltan Ivanfi
    
    BUG: 402708
    FIXED-IN: 5.0.3
    
    Reviewers: #kmymoney, tbaumgart
    
    Reviewed By: #kmymoney, tbaumgart
    
    Subscribers: aacid, tbaumgart, ostroffjh
    
    Differential Revision: https://phabricator.kde.org/D17871

commit 5cf4c62fd0109b6ab8b42d3429e96b361a2e5ee0
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Jan 14 14:03:57 2019 +0100

    Fix finding of alkimia version 7 with custom install path
    
    CCBUG:403156
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D18242

commit 82af18232bb780e5117318a4dc487c3638085a9e
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Jan 14 14:03:35 2019 +0100

    Fix cmake warning on finding alkimia package with different minor version
    
    FIXED-IN:5.0.3
    BUG:403156
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D18241

commit 3939181ec1ce65de5c13867e73e119cbcefca8b7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Jan 13 09:17:21 2019 +0100

    Fix customized report loading and saving
    
    The storage of the accountgroup information for customized reports was
    broken due to a mis-assignment between numeric and textual
    representation. This was introduced with
    https://phabricator.kde.org/D13581 which landed as commit
    b7c44b7f705cd8.
    
    With the current change, the account group information will be
    reconstructed during loading of the file depending on the report type
    and the above mentioned assignment problem will be fixed.
    
    Also, the report type was not written for any customized information
    report which caused a crash during usage after the next start of the
    application. Since the report type cannot be detected automatically from
    the available data in broken files, the type will be set to AccountInfo.
    
    A respective message is dumped to the console. Users may have to
    redesign their loan, schedule and schedule summary reports. The problem
    shows, if a user opens such a report and it now displays an account
    information report. Nevertheless, the original report filter settings
    are loaded correctly.
    
    All other report types are not affected by this problem.
    
    BUG: 403068
    FIXED-IN: 5.0.3

commit a606f027f9dade40b270636dc46cfc0d33340ed2
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Jan 9 20:17:41 2019 +0100

    Fix investment transaction editor
    
    The investment transaction editor did not handle all fields according to
    the selected transaction type. This change fixes these issues.
    
    BUG: 403039
    FIXED-IN: 5.0.3

commit 97e3186babf1d4426f5fbd2cd9c9a6da459ea37d
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jan 3 17:34:19 2019 +0100

    Fix transaction register display in tags view
    
    In case a transaction had a tag assigned, an amount differing from zero,
    a category assigned but no payee, the register in the tags view showed
    wrong results. This fixes the behavior so that the memo is shown instead
    of the erroneous text "*** UNASSIGNED ***"
    
    BUG: 347685
    FIXED-IN: 5.0.3

commit 6ae4b64acfcdbd1b1d2ff09fbb022957fe8aa647
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jan 3 10:47:59 2019 +0100

    Fix initial state of "View/Show all accounts"
    
    In some circumstances, the initial display of the "View/Show all
    accounts" option was not correct. This change sets the initial state
    according to the setting after program start.
    
    BUG: 402814
    FIXED-IN: 5.0.3

commit c4bc9a09fb10b83567ea5ee0a297ceb1cdc9a0b7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jan 3 10:35:15 2019 +0100

    Show closed accounts in report configuration
    
    In case the configuration of a report references at least one closed
    account, also closed accounts will be shown in the configuration widgets
    
    BUG: 397467
    FIXED-IN: 5.0.3

commit b0ddc9f4510e4f2390b98524e891a09a0c6206e6
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Jan 3 08:55:47 2019 +0100

    Update tax in multi-selection edit session
    
    In case the user edits multiple transaction at once and assigns a
    category which has a reference to a tax-category, the relevant tax split
    is created for all transactions and the values are updated accordingly.

commit e366cd56856d668835fbdfa1d3e50b935a1ac160
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Jan 3 08:19:19 2019 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours
    
    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit f838e3c0b7b3cd5543af3ce4a6119a9a41b2fcba
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Jan 2 18:23:02 2019 +0100

    Automatically adjust tax split if possible
    
    Importing transaction assigns categories based on previous transactions
    found with the same payee or a preselected category. In case such a
    historic transaction contains multiple splits then these splits are
    copied but the amounts of the splits are not adjusted based on the value
    of the imported transaction.
    
    This change now adjusts in case the resulting transaction consists of an
    asset/liability account, a taxable category and a tax category such that
    the sum of all splits is zero based on the imported transaction.
    
    This also fixes the "Copy splits" feature when used on a taxed
    transaction.
    
    BUG: 402794
    FIXED-IN: 5.0.3

commit b7a3cebd6ec88fe5a0d31bca58b9f3a6fd47d2de
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Jan 2 13:03:32 2019 +0100

    Destroy transaction editor if still existent when closing file
    
    A leftover transaction editor is not a good idea. A better mechanism
    would be to disable any file action while an editor is active but that
    will be a larger change to be considered in the future.
    
    BUG: 402783
    FIXED-IN: 5.0.3

commit f800f7666ee31f64f8d17bbf4c51585e006c4922
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jan 1 17:34:18 2019 +0100

    Don't change stock split factor by rounding
    
    Rounding of the stock split factor may cause the factor to become zero
    which will later result in a division by zero. The factor must not be
    rounded to solve the problem.
    
    BUG: 402750
    FIXED-IN: 5.0.3

commit daa58f3a993bdd55db094ec148d9dbcbe05963c6
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jan 1 14:33:21 2019 +0100

    Fix dynamics of investment transaction editor
    
    The dynamic appearance of widgets depending on the selected activity was
    completely broken. This caused trouble on the tab-order which was
    reported.
    
    BUG: 402694
    FIXED-IN: 5.0.3

commit 7b21ad93648315003a1f3f56cd55ba830dedad8a
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Jan 1 14:28:15 2019 +0100

    Removed unused code

commit 3cace77952a9e3390160c51448a1587e71f04bbd
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Dec 31 16:40:15 2018 +0100

    Don't send engine notifications for removed objects
    
    It could happen that modification notifications were still pending for
    removed objects. One such instance was caused by removing whole account
    hierarchies at once. This change fixes the problem.
    
    BUG: 402699
    FIXED-IN: 5.0.3

commit 1bc7f6a44a696b8625c4bd89df5d6a0f83071e30
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Dec 25 20:36:38 2018 +0100

    Fixed typo

commit 647c856a193337e00b0af309ec9476292c1efaee
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Dec 25 20:36:12 2018 +0100

    Fixed comments to contain correct names for the context

commit 0b79fc5d74088029ff67f57ebe3b52fb285b1d51
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Dec 25 20:34:55 2018 +0100

    Remember the full URL for saving the database
    
    When using the 'open recent' feature on a database, the password was not
    remembered and any following save operation of the data failed. This
    change remembers the password for that operation.
    
    BUG: 402534
    FIXED-IN: 5.0.3

commit 804203bab3c51d0d31a169e33f1d0ba47ab64da3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Dec 25 08:54:45 2018 +0100

    Clear database table before refilling it
    
    BUG: 402547
    FIXED-IN: 5.0.3

commit ca014fc7640d4b3e20bc12f93bd061d80c6b8f27
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Dec 23 16:02:28 2018 +0100

    Import number field during CSV import
    
    BUG: 402501
    FIXED-IN: 5.0.3

commit 0a22879865008f48716f9fba9b8b39043da4e8d3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Dec 19 18:36:32 2018 +0100

    Support creation of transaction from any view
    
    FEATURE: 402316
    FIXED-IN: 5.0.3

commit 7c6b5a8e02e8a9eaacc68a56e38886dc2a4b95f1
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Dec 19 09:15:06 2018 +0100

    Don't post superfluous question
    
    In case the creation of a new transaction is started but no data was
    entered and the user clicks on an area outside of the transaction editor
    a confirmation message popped up asking if he wants to save the data
    (when there was none). This has now been modified so that the editor is
    silently closed in case the 'Enter' button is disabled.
    
    As a side effect, one of the questions was adjusted to reflect the
    actual situation.
    
    BUG: 402302
    FIXED-IN: 5.0.3

commit 15bfa066212958a25804ab9c3b9b4891701c4a75
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Dec 18 22:02:50 2018 +0100

    Support building against next version of libalkimia
    
    In case the next version of libalkimia is installed and found it will be
    used. Currently the interface is still identical and it can be used
    without problems.

commit c21f3048ad686981352686b8d2da58e5c326278a
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Tue Dec 18 22:00:41 2018 +0100

    Correctly read opposite signs option from banking profile
    
    The "Opposite signs" checkbox is not being initialized from the value
    stored in the Banking profile in csvimporterrc. The value is stored
    correctly in csvimporterrc as OppositeSigns=true but the checkbox is
    always unchecked when the profile is re-used. This change fixes the
    problem.
    
    Thanks to Mark for providing the fix.
    
    BUG: 402195
    FIXED-IN: 5.0.3

commit 32dc55b163df1d3e48f17a6d660db3d00153ac20
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Dec 17 17:25:57 2018 +0100

    Fix display behavior of Enter Schedule Dialog
    
    - Remember width of dialog
    - Place centered on screen
    - Increase default size
    
    BUG: 402200
    FIXED-IN: 5.0.3

commit 349f695d20bb95a0d0670e9214536f98b8de2115
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Dec 17 17:25:28 2018 +0100

    Don't include unused header file

commit 82f479c6f4e97646ec0fad6c3919289bfa499306
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Dec 17 08:00:44 2018 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours
    
    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit a9c6fffc9cccd8718f93239b98faa6628f2835f5
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Dec 15 12:03:21 2018 +0100

    Unselect item in case text field is empty during focus out
    
    In case the combo box contained a previously selected item, this was not
    reset when the combo box text field was empty and the focus was removed
    from the field. This caused the previously selected item still to be
    reported as current item during e.g. the storage operation of a
    transaction.
    
    BUG: 402120
    FIXED-IN: 5.0.3

commit dec695ae9040807ff011b8a16fc1d4065ba215f8
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Dec 12 21:08:28 2018 +0100

    Added more application version ids for OFX

commit c74bde8a1599e30e624a1919fc66a59a592d8873
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Thu Dec 6 12:34:45 2018 +0100

    Fixed bug that crashed program when clicking Forecast->Chart
    
    BUG: 400820
    FIXED-IN: 5.0.3
    
    Reviewers: #kmymoney, tbaumgart
    
    Reviewed By: #kmymoney, tbaumgart
    
    Subscribers: tbaumgart
    
    Differential Revision: https://phabricator.kde.org/D17368
    
    (cherry picked from commit 6ba86e8d02dc3dc7fc57b66097a5803c96c22571)

commit b2300d094c8c281d18732b1d4c09ac05c22289cc
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Dec 2 07:11:55 2018 +0100

    GIT_SILENT made messages (after extraction)

commit e29ab436caac3b2eda310b7b3d201831eddf25f8
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sat Dec 1 13:52:46 2018 +0100

    Make sure to display full title on charts
    
    Upstream library software (kdiagram) causes the title of a chart to be
    chopped off on the right side as shown in attachments to bug #399260. I
    used gammaray to see that the full title name was passed to the drawing
    routines inside kdiagram but still it got truncated. Adding a few extra
    spaces (5 in this case) at the end solved the issue.
    
    BUG: 399260
    FIXED-IN: 5.0.3

commit fc251a95c40d393fc13a12619434240addd3bb6f
Author: l10n daemon script <scripty@kde.org>
Date:   Fri Nov 30 08:33:28 2018 +0100

    SVN_SILENT made messages (.desktop file) - always resolve ours
    
    In case of conflict in i18n, keep the version of the branch "ours"
    To resolve a particular conflict, "git checkout --ours path/to/file.desktop"

commit fd5779c0f84a8de8bf8cf746699d94fdac3def36
Author: l10n daemon script <scripty@kde.org>
Date:   Fri Nov 30 07:25:40 2018 +0100

    GIT_SILENT made messages (after extraction)

commit 85202855788b3cdb74ae79d142f96266cdcd861e
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Thu Nov 29 09:27:28 2018 +0100

    Minor bracket placement fix
    
    Fixup for commit 4e83c38ab.
    
    CCBUG:401080

commit 4e83c38abb4ff5b558c0f904a01a7f2b1ab76f6a
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Thu Nov 29 09:10:45 2018 +0100

    Add support for opening or printing of multiple reports
    
    In the report overview select multiple reports and select one of the
    available entries in the context menu opened with right mouse click.
    
    Cherry-picked from branch 4.8, commit bf6278f92.
    Adjusted for KF5 by Thomas Baumgart.
    
    BUG:401080
    FIXED-IN:4.8.3, 5.0.3
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D17231

commit 546e5e7cf16bd8adf9a477345a0e0c83b2c73166
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Tue Nov 27 12:55:15 2018 +0100

    Show an empty column for the return on investment if the value cannot be calculated
    
    There is a difference between 0.0% return and values, that are not
    calculable because some prerequisites may not be fulfilled.
    
    Cherry-picked from branch 4.8, commit 5816a0a18.
    
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D17183

commit 73a2c94feef3b522fa383322c726e7d1cfa80180
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Nov 26 10:23:37 2018 +0100

    Fix msvc compiling issue
    
    std::isinf() and std:isnan() is supported only for msvc >= 2017.
    For msvc < 2017 related definitions has been added to kdewin
    version 0.6.3.
    
    BUG:401448
    FIXED-IN:5.0.3

commit a7683b5440c7956084e96e62ae4e07bf5dfc3296
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Nov 26 09:55:41 2018 +0100

    Save account attribute 'Include in tax report' when exporting account structure to KMymoney template file
    
    CCBUG:380286
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D16727

commit 4735e228ae9ac31fc51d353007aad65d2c6e4e38
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Nov 26 09:55:29 2018 +0100

    Add support for saving and loading tax account mappings to and from a template file
    
    A tax account mapping is stored in an xml tag of type 'flag' with
    the attribute name='VatAccount'. The attribute 'value' contains a
    reference to the corresponding tax account, which contains the
    reference in the attribute 'id'.
    
    When importing a template file with a tax account assignment, the
    reference is first stored in a temporary key/value pair with the
    name 'UnresolvedVatAccount' of the account concerned and after
    completion of the load is stored in the key/value pair 'VatAccount'
    based on the ID of the tax account now available.
    
    CCBUG:380286
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D16726

commit 757f42ff80a787550392192226287ec79c515459
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Mon Nov 26 09:47:40 2018 +0100

    Export account attribute 'VatRate' into a template file and read out again when importing
    
    CCBUG:380286
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D16725

commit f580ae94efa58bfbe96cb5418cb12d717f83df2b
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Nov 25 17:18:55 2018 +0100

    Don't update views if no storage element is attached
    
    In case of WebConnect and tabbed view views become active during
    destruction of objects with no storage attached. This change prevents to
    update the views in this case causing the crash.
    
    BUG: 396831
    FIXED-IN: 5.0.3

commit 7e6cfca312659bb07c069fc27665a7b614dd351f
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Sun Nov 25 13:30:36 2018 +0100

    Correction of setting up import of tax attributes from gnucash xea file
    
    KMyMoney expects the flag "Tax" to have an attribute value="Yes".
    
    BUG:380286
    FIXED-IN:4.8.3, 5.0.3
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D16728

commit b87e02c13e8eb027211d0e69862d9c4370653025
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Sun Nov 25 11:19:52 2018 +0100

    Corrections for XIRR implementation to achieve more accurate values
    
    In the Investment Report, the XIRR() function is used to calculate
    the annual return. The previous implementation contains some errors
    that have been corrected with this commit. In addition, if XIRR
    cannot calculate the annual return to notify the user of this case,
    an empty column is displayed in the investment report.
    
    The basis for this fix is an update of the XIRR implementation from
    the KOffice project [1], which brings the calculation up to date and
    a further improvement from the libreoffice XIRR implementation [2] to
    get more solutions using a two-pass approach. The class CashFlowList
    has been moved to separate files to have a cleaner separation.
    
    The obsolete and unused NPV function has been removed.
    
    Tests for XIRR have been extended to check for recognized issues.
    
    [1] https://github.com/KDE/koffice/blob/master/kcells/functions/financial.cpp
    [2] https://raw.githubusercontent.com/LibreOffice/core/master/scaddins/source/analysis/financial.cxx
    
    BUG:385993
    FIXED-IN:4.8.3, 5.0.3
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D16766

commit e3ad0e31ff48336e605263fdbef5e40c0f4550d7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Nov 18 08:35:42 2018 +0100

    Support price import in exponential format
    
    BUG: 401144
    FIXED-IN: 5.0.3

commit af4ffb8913631d9b7b2224f9fe690f395120a53b
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Fri Nov 16 12:03:57 2018 +0100

    Add status entry for showing scheduled transactions in ledger view
    
    This is useful to see if any scheduled transaction has not been entered
    especially in the case that the 'Display schedule transactions with planned
    post date' is switched on.
    
    Cherry-picked from commit 2502a10da/review D16911 and adjusted to branch 5.0.
    
    BUG:401079
    FIXED-IN:4.8.3,5.0.3
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D16916

commit d817957f04ab633d19402568c210bb5a686773ff
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Thu Nov 8 22:51:17 2018 +0100

    Fix gcc warning: declaration of ‘task’ shadows a member of ‘onlineJob’ [-Wshadow]

commit 76216f4288f7a55371ff3143cef5c4dc4d048140
Author: Ralf Habacker <ralf.habacker@freenet.de>
Date:   Tue Nov 6 17:54:45 2018 +0100

    Add feature to display schedule transactions with planned post date
    
    If a planned transaction is overdue, today's date is used by default as
    the posting date. With this option, the originally planned date is
    used instead.
    
    BUG:398919
    FIXED-IN:4.8.3, 5.0.3
    Reviewed By: tbaumgart
    Differential Revision: https://phabricator.kde.org/D15746

commit be3dfc7d78cda1794a4597abe7f0fa13b5a375bc
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Nov 5 16:36:53 2018 +0100

    Provide all arguments separately
    
    All arguments to the copy command need to be presented as their own
    element in the argument list. The current code resulted in an invalid
    source filename with "+ nul" appended.
    
    CCBUG: 302945

commit d3efec9f58610f5a2928570543128cd8b99151df
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Nov 4 18:05:21 2018 +0100

    Add debug output to see backup command
