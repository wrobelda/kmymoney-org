commit 962a2b212b049ba91e0b947aef86a6b1af954449
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Fri Oct 31 19:57:03 2014 +0200

    Bumped version number to release 4.7.1.

commit bd1513090df2e8460243e7f63eb61c3488a9e106
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Oct 26 05:24:54 2014 +0000

    SVN_SILENT made messages (after extraction)

commit 03f4c27a29f5fc6427f48344d013a4ae966400b4
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Oct 5 11:00:57 2014 +0200

    Update model completely when account data changes
    
    Selecting to include a category in the tax report by marking the
    checkbox in the account dialog presents a check mark in the category
    view. Turning off the feature by unselecting the checkbox in the
    account dialog did not remove the check mark in the category view.
    This fixes the behavior.
    
    BUG: 339693
    (cherry picked from commit cf16426575844043c6b1c4638e22582e0c799d1f)

commit 233bcf403226cf669cdae22cff211ef29a22a5a9
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Oct 5 09:22:23 2014 +0200

    Display assigned VAT account in list view
    
    The category view contains a column called VAT which showed a check mark
    when the category in question has a VAT category assigned. Now the name
    of that VAT category is shown instead of the check mark.
    
    FEATURE: 339655
    (cherry picked from commit f803f3f9ee6cd82b1d0fab5fe0e69ab368ac0ebe)

commit 4fc6fbbf314d007b53445b084cab2e095f51ccdd
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Tue Oct 21 07:53:29 2014 +0300

    Fix security matching on import, don't match empty fields.
    
    Use the same policy of matching two fields only if they are not
    empty consistently for matching securities. Thanks to George
    (george@wildturkeyranch.net) for figuring this out.
    
    BUG: 337208
    (cherry picked from commit 8299e4f37d6fdea9723501acec416d2e883a9a8a)

commit 2d64363d9c8d8ad7496a7e6d0db8a86c5ca1e84e
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Oct 19 17:16:37 2014 +0300

    Fix opening OFX files containing non ASCII characters.
    
    Use QFile::encodeName to obtain a filename which can be opened by libOFX.
    
    I haven't been able to test this yet on Windows but I'm sure it will fix
    the reported issue.
    
    BUG: 330437
    (cherry picked from commit f24601327125557a9c5ea455d18842828cdd5c2f)

commit dea582710293e36cbeaf049d6a7ca58dca6b5e3d
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Oct 19 14:09:26 2014 +0300

    Fix transaction entry when starting from transfer tab.
    
    When the transfer tab was being disabled the next tab was being
    set as the active tab automatically without considering currently
    entered data. This would mess up the transaction as described in the
    report.
    
    To fix this, before disabling the transfers tab, if it is the current
    tab, the proper deposit/withdrawl tab is selected.
    
    BUG: 300697
    (cherry picked from commit 3af05653526bbb8189e21dcccdd3f5331723e932)

commit 41029ceb3662cc687ad77224d24e788d14de54db
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Oct 19 12:16:19 2014 +0300

    Properly import OFX investment transactions.
    
    Since fixing a previous issue the amount is no longer negative for
    investment transactions so, to obtain the total, the fees need to be
    added (have the same sign as the amount).
    
    Now the provided file is imported without triggering the missing
    assignment error. This was caused by miscalculated values, the shares
    were properly imported.
    
    BUG: 339192
    (cherry picked from commit b9bfdf3f338492f64119958970f70e4610d103aa)

commit f773f22f2685f7ae6e5bbafd461873cdcc08e349
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Oct 18 20:41:55 2014 +0300

    Fix the currency conversion dialog for really big/small conversion rates.
    
    If the to value was equal to zero when the dialog was constructed a bad
    (inverse) conversion rate was applied. This code was there since version
    1.5 (on CVS) of this file. I don't understand why it was added that way
    but now it's fixed.
    
    Also fixe the prices dialog to display the prices using the configured
    price precision.
    
    Now in case that the value after conversion becomes zero the transaction
    can't be entered.
    
    BUG: 334564
    (cherry picked from commit 7f61ffb728b58bbb905bba87e184e221a7d9858a)

commit e3b4e55b2fcf9ad36ae5a397fe9f016d4a480039
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Oct 18 16:42:26 2014 +0300

    Remove the too harsh transation entering restrictions.
    
    Now a transaction can be entered if at least one of the
    following fields payee, category, amount is valid. This
    was the old behavior and it seems better to be more
    permissive with the user as long as no harm is done.
    
    Attached the commit to the original bug report which seems
    invalid since it requests these restrictions.
    
    BUG: 314955
    (cherry picked from commit cc7354d043efd0da84f8eee004b7b0c5390422dd)

commit 1a76540fbc936dd6e80cfe9faaeef7b35d0c166b
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Oct 18 15:57:35 2014 +0300

    Fix the validation of investment transactions.
    
    Don't ajust the values to the smallest account fraction of the
    currency. This will cause roundings that will lead to the reported
    validation error.
    
    Because at 0.5 AlkValue::RoundRound can round up/down depending on
    the result it could be that one split it rounded up while the other
    is rounded down leading to a 0.1 error while summing them.
    
    Having a value which is more precise than the smallest account
    fraction should not cause any issues since the UI should render
    the value according to this fraction anyway.
    
    If rounding is necessary a fix rounding method should be applied
    for all splits which are going to be summed up.
    
    BUG: 303026
    (cherry picked from commit 1506b0ccacb9d4efa2b6aaeb725c2e73f5507d83)

commit eceae0265be3a8e2d32aa90ae1a5bca487e48078
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Oct 18 13:10:54 2014 +0300

    Don't update the amount when multiple transactions are selected.
    BUG: 332793
    
    (cherry picked from commit 1a05ccce75b9cc0c47adeaefa2172a647e42d77d)

commit 15de2abfa0a2debaea61645d62895d87fb32473d
Author: l10n daemon script <scripty@kde.org>
Date:   Wed Oct 15 05:14:36 2014 +0000

    SVN_SILENT made messages (after extraction)

commit 9d01b40c0f34b034709ca3191130a8e2a0338c94
Author: l10n daemon script <scripty@kde.org>
Date:   Sat Oct 11 05:30:47 2014 +0000

    SVN_SILENT made messages (after extraction)

commit 7f12d0f3c9c3a45ac9d10d593b218423299c5511
Author: l10n daemon script <scripty@kde.org>
Date:   Fri Oct 10 06:00:57 2014 +0000

    SVN_SILENT made messages (after extraction)

commit 6df806aef9633b365b8a42ea6ee2a6c7f2174992
Author: l10n daemon script <scripty@kde.org>
Date:   Thu Oct 9 05:18:16 2014 +0000

    SVN_SILENT made messages (after extraction)

commit e50daf102574f3b3c2679d441a143b9f0e58b5dc
Author: l10n daemon script <scripty@kde.org>
Date:   Wed Oct 8 05:20:56 2014 +0000

    SVN_SILENT made messages (after extraction)

commit bbb0f8cb8b130ea3fe30bb0b43b709b2d58b0c30
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Oct 5 10:51:09 2014 +0200

    Don't use pointer in case it is 0
    
    Make sure to check the pointer to the dialog object first before using it.
    
    (cherry picked from commit 9106cf2b019781a252f0092a9b77cad08f8e5ccf)

commit 93ee5db9412c118e39ce20fd97660dd6f9b56a14
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Tue Oct 7 23:29:46 2014 +0300

    Fix the platform dependent condition checking.
    
    BUG: 331508
    (cherry picked from commit ed66cd2e87fa868dd80d6eda5be82e98bbcd04af)

commit 90182f8454310ddd8d7745682ac5dbcbebd1ba1c
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Tue Oct 7 22:17:31 2014 +0300

    Fix the context menu triggering with multiple selection on Windows.
    
    This is a Windows only fix but it does not require the code to be
    added for Windows only since, as the comment states, on Linux the
    condition will never be fullfiled.
    
    BUG: 338298
    (cherry picked from commit 0e89408581faa2cee88740961c4fdd69678d0c52)

commit 7d77d244871027e6afb5532ee3fe13ec81eb04ab
Author: Rex Dieter <rdieter@math.unl.edu>
Date:   Tue Oct 7 08:18:44 2014 -0500

    Fix appdata validation
    
    previously...
    $ appstream-util --nonet validate-relax kmymoney/kmymoney.appdata.xml
    kmymoney/kmymoney.appdata.xml: FAILED:
    • style-invalid         : <caption> cannot end in '.'
    • style-invalid         : <caption> is too long
    Validation of files failed
