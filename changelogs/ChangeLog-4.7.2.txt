commit 9fcdbe15ec822bf2bc248e1322fb1116993ba8af
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Apr 25 19:33:20 2015 +0300

    Bumped version number to release 4.7.2.

commit 7bcbaf078cafb45a65612a6f6986a7569cbab803
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 19 08:47:46 2015 +0200

    Replace usage of 'echo' with 'true' during backup
    
    The backup function used the echo command as a nop command which returned
    an error when KMyMoney was started in a xterm console as background
    process and the console was quit before the backup operation was
    started.  Using the true command instead solves the problem.
    
    (cherry picked from commit e1bc8e11f85cccf3e1e2a82bfc050b604ca8b37b)

commit 4bd2cc7ea7a534b9ca1e1a5f6ea0bc396971ee32
Author: l10n daemon script <scripty@kde.org>
Date:   Sat Apr 18 08:11:34 2015 +0000

    SVN_SILENT made messages (.desktop file)

commit 78d218a510aac075f91a4ef2bf0ac5e290658f69
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Mon Apr 13 13:01:10 2015 +0300

    Fix typos: data -> date

commit d5b5a852e4d9deec1e45bd3fd04c895c12b7ed88
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 12 14:19:16 2015 +0200

    Don't modify transaction date automatically
    
    The recently introduced consistency check feature to modify the
    transaction date to always make sure to be post the opening date of any
    referenced account may cause some serious trouble when operating on
    historical data entered using previous versions of KMyMoney. This could
    have the effect that transactions get a post date even in different
    years. This certainly causes severe problems with the financial figures.
    
    This change prohibits the automatic change of a transactions post date.
    In case the date problem is caused by a category or stock account, the
    opening date of the account is modified to the post date of the
    transaction. In case an account is causing the problem, the user will
    receive information about the name of the account and the transaction in
    question. It is left to the user to fix the problem manually.
    
    In rare cases, the problem cannot be fixed since by modifying the
    opening date of the account, as the current UI does not support to
    change it. This is true for e.g. loan accounts.
    
    This implementation operates a lot safer on user data than the original
    one. I had reviewed the code and found it to be OK, but did not see the
    side effects that I found while running the consistency check on my
    production data.
    
    (cherry picked from commit af7939608b3aa305a86b0ad6196beacb52c63108)

commit dc20547c3d42c11c3eadb4397964ff990d6ef4c3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Sun Apr 12 08:30:28 2015 +0200

    Allow editing of opening date for categories
    
    Up to the recent change in the transaction editor to verify that all
    opening dates of all referenced accounts/categories are prior to the
    transaction date, the opening date of a category did not play a role
    and was not editable. It was set to the date the category was created.
    
    This creates a problem, in case I want to enter a transaction with
    post date yesterday using a new category I created today.
    
    Therefore, we simply make the field editable. Also, the standard
    opening date for categories is now the first day of the current
    fiscal year instead of the current date.
    
    (cherry picked from commit a8d0a2fcb7580399c2c553b448e6536e3485a535)

commit 0ce50018f87b2e19386001ac2f141aafb7119a44
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Fri Feb 27 21:47:29 2015 +0200

    Add a stage to the consistency check to fix transaction post dates.
    
    If the transaction is moved to another post date the price information
    is moved along with it for investments transactions. This fixes the
    reported issue, saving the file once will fix all price issues.
    
    Also add the possibility to save the consistency check log to a file
    or copy it into the clipboard.
    
    Make sure that the user enters a valid transaction date by checking
    that the transaction date is after the opening date of each and every
    account involved in the transaction. Until now we only checked that
    the transaction date is after the opening date of the account in which
    is being entered.
    
    Remove the check for "Sell" action since there is no such action.
    
    BUG: 313793
    REVIEW: 122746
    (cherry picked from commit dbd547a503e14e146d7f9d2b1b0781f05ad3e0b6)

commit f303fcb68955437fd1371a68bd914190f585daf2
Author: Allan Anderson <agander93@gmail.com>
Date:   Fri Apr 3 23:23:34 2015 +0100

    BUG:345781
    Fix Cheque number in kmymoney/dialogs/kenterscheduledlg.cpp not
    auto-incrementing.
    
    (cherry picked from commit 2c1c35ff15385044e616aba0e1a98d81974eae60)

commit 6fed3349e20c2ce9898058b036b3a20ec90ad033
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Mar 29 20:37:40 2015 +0300

    Properly handle loan payments from accounts using a different currency
    
    If a payment account that has a different currency than the currency
    of the loan is selected in the loan wizard, the resulting scheduled
    transaction would not have the proper value in the currency of the
    payment account.
    
    Fixed this by requesting the user to perform the currency conversion
    from the amount of the payment in the currency of the loan to the
    amount in the currency of the payment account. This conversion is done
    using the standard mechanism provided by the currency calculator.
    
    After this was fixed I observed that the amount of the scheduled
    transaction was not being rendered properly in the homepage. Fixed
    this by using the same amount formating that is used in the ledger.
    
    BUG: 325793
    REVIEW: 123177
    (cherry picked from commit d87427c4e8fad8226c1f1a80bbb094d9cc59a2ab)
    
    Conflicts:
    	kmymoney/views/khomeview.cpp

commit 22bb28ada10f5844084a2c6ee1c864262d2fa62a
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Mar 29 21:20:05 2015 +0300

    Fix the loan wizard losing the currency of the loan.
    
    If a loan is created in a different currency than the base currency
    and a new payee is created for the loan then the currency of the
    loan ends up being the base currency instead of the originally
    selected currency.
    
    This bug was caused by some code which didn't made any sense (set an
    invalid selection in the currency combo upon file change) so I just
    removed it to fix the problem.
    
    BUG: 345669
    (cherry picked from commit aa330f9412c90f5161ff5b91f330cbc031a3f8c8)

commit 34d20dfbc76ac89d3a44cfebc39bdc951dc2326a
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Mar 28 12:40:17 2015 +0000

    BUG:340656
    REVIEW:122364
    CSV Importer - Fix display on high-definition monitors.
    Fix interaction between QTable and QWizard in same window.
    Add a number of edits from Reviewboard.
    
    (cherry picked from commit 0dc7e903d389c458289a92634f9e346928316173)
    
    Conflicts:
    	kmymoney/plugins/csvimport/csvdialog.cpp
    	kmymoney/plugins/csvimport/investprocessing.cpp

commit 7dd5fde7ace481635e65d8e7d7616f93902f0a31
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Dec 13 17:07:56 2014 +0000

    BUG:330737
    Fix problem in commit 00ec9fa1d3860f7ce4f9d3ba46d8a7a6ba1ee329.
    Selecting tab as field separator resulted in colon being used, even
    though the UI did not present the colon as a possible selection.
    
    Please enter the commit message for your changes. Lines starting
    
    (cherry picked from commit 9f8f78edeba974b01c762bb9c5e75bfd714a85e5)
    (cherry picked from commit 5f85c0074bfd9dd4cbd3743ee492d055f602c570)

commit d6dc17bdea7f26cd9e817320c050d0528a6eb6df
Merge: b76869b e3967e5
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Mar 29 14:25:16 2015 +0100

    Merge branch '4.7' of git://anongit.kde.org/kmymoney into 4.7

commit b76869b7b22645edbce617541dbe161e6b557d99
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Dec 13 00:09:06 2014 +0000

    BUG:330737
    Fix problem with hard-coded field separators, by adding selection to the UI. The use of a colon as field separator would clash with the category:subcategory use, so is excluded.
    
    Export of Reinv and Div transactions is improved.
    
    (cherry picked from commit 00ec9fa1d3860f7ce4f9d3ba46d8a7a6ba1ee329)
    
    Conflicts:
    	kmymoney/plugins/csvexport/csvexportdlg.cpp

commit e3967e5918ba887c69e4de809605072686bc2f91
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Mar 29 15:48:56 2015 +0300

    Fix foreign currency income/expense transactions in reports.
    
    The previous commit broke the convert all values to the base currency
    use case. Fixed this by properly checking that flag before converting
    a split's value to the currency of the starting account.
    
    Extended the multiple currencies report test case to check this
    scenario.
    
    BUG: 345550
    (cherry picked from commit 6780bf61922eab24542f5d02b29a17b0df12299d)

commit a6a28f45ecaac6f80496f47c5230cf41ae286a09
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Sat Oct 18 08:45:37 2014 +0300

    Fix typo
    
    (cherry picked from commit 076c15f9b114f3accfb534a6ba5bd98fff1bdd00)

commit a4946c87e51e031812160471dae8933b0c0f04d5
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Dec 6 12:35:34 2014 +0000

    BUG: 322381
    Fix fees(taxes) being deducted twice from dividend amount during QIF import.
    Also fix imported amount being negated.
    
    The former appears to be a very long-standing problem.
    
    The latter is a consequence of the fix for BUG 333522 5c54aafef7aa8a39ffd3b5e2dcb6538301652ee5.
    
    (cherry picked from commit e09e3467305aa926ba49852638b44ceb8089c318)
    
    Conflicts:
    	kmymoney/converter/mymoneyqifreader.cpp
    	kmymoney/plugins/csvimport/investprocessing.cpp

commit b458327e70a3bcbc214b904b48bb66398a6877a9
Author: Allan Anderson <agander93@gmail.com>
Date:   Thu Oct 16 12:39:23 2014 +0100

    CSV Plugin - Improve functionality across distros. Improve UI to display the whole of the file being imported, and smarten to avoid displaying split rows. Also, rework some areas of the code to avoid duplication and improve re-usability. In addition, the UI heading labels have been pruned, to simplify translation. They are still centered, but no longer HTML based.
    
    (cherry picked from commit 50f7b6c47ad4f326b7cd6b2e0f8746b8391b3810)

commit 7b10760b947432e77078ade8d6ef1557321b3a74
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Fri Mar 27 19:08:41 2015 +0200

    Fix foreign currency income/expense transactions in reports.
    
    Since commit 9fc583d3db4fe2c16448790d7ee17145fbe8bd2f was added as a
    fix for some currency conversions performed in reports income/expense
    transactions which involved a currency conversion were not properly
    displayed in the transactions by account report.
    
    Extended the multi-currency report testcase to catch this error and
    fixed the problem by resurrecting some conversion code which was
    removed by that commit, but moved it to a place where it's applied
    only to income/expense transactions.
    
    BUG: 345550
    REVIEW: 123154
    (cherry picked from commit 31adc38e7d632fb79eb3163998c8c568ee957383)

commit 7e7a50e39170d03c9fe83ffcbe394d485cefaf52
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Mar 15 12:14:13 2015 +0000

    BUG:333949
    Remove restrictions on matching of any imported or manual transactions.
    Tidy up.
    
    with thanks to w00dp@mail.com for the initial patch.

commit 61ea11eedf29e5f0a1a518f309ce6f35e495d82e
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Mar 15 12:14:13 2015 +0000

    BUG:333949
    Remove restrictions on matching of any imported transactions.
    
    with thanks to w00dp@mail.com for the initial patch.

commit 2c8ef6d46b0c20880152482600dd91c57c61b07f
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Mar 15 12:14:13 2015 +0000

    BUG:333949
    Remove restrictions on matching of any imported transactions.
    
    with thanks to w00dp@mail.com for the initial patch.

commit cefde1bb04e87b7889f00917361bbb88a712f872
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Mar 15 12:14:13 2015 +0000

    BUG:333949
    Remove restrictions on matching of any imported transactions.
    
    with thanks to w00dp@mail.com for the initial patch.

commit 48cc7c560fc989e2437724217f78c0992ca5026a
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Mar 15 12:14:13 2015 +0000

    BUG:333949
    Allow matching of two non-imported transactions.
    
    with thanks to w00dp@mail.com

commit e5f2d880755e6af528ebbf0550fda7adafe7e96e
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Fri Mar 20 08:47:36 2015 +0200

    Do not use translation puzzles

commit ea9a548272903ec657993eba011c6e72695d632d
Author: Allan Anderson <agander93@gmail.com>
Date:   Sun Feb 8 22:09:30 2015 +0000

    BUG:343106
    Fix unhelpful messages when processing schedules at startup.
    Fix 'next check number' message when updating a schedule for a
    non-checking schedule type.
    
    (cherry picked from commit 71bbf7b53125edfd37047f0bdba1feedd88a6d26)
    
    Conflicts:
    	kmymoney/kmymoney.cpp

commit 987a9903bcb3cdb1567d3cfbe495d495faf1770a
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Wed Mar 4 07:22:32 2015 +0200

    Fix typos

commit 075cf48505bf1d5714d94378e28770ecbbb8e61a
Author: Allan Anderson <agander93@gmail.com>
Date:   Fri Feb 6 17:43:08 2015 +0000

    BUG:343757
    Fix crash when importing CSV investment file after editing settings.

commit 9dab50bf3948488bd0281874eee71a75efa9bcde
Author: Allan Anderson <agander93@gmail.com>
Date:   Fri Feb 6 17:43:08 2015 +0000

    BUG:343757
    Fix crash when importing CSV investment file

commit 626c3d4e8d9d1910e1a3f2a07e4c5115de197aac
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Fri Feb 27 19:33:00 2015 +0200

    Improve the code in the investment transaction editor.
    
    Actually it's pretty poorly written, I don't really know about the
    investment editor's requirements so this just makes it safer.
    
    I was not able to reproduce the reported crash in the 4.8 branch but
    I think that this improvement is necessary.
    
    BUG: 342181
    (cherry picked from commit 8bae63bfd82a7e7f63277ab515e8f3cd85ab1423)

commit 046344e37c05a9e4da14599b994316cbc1c72dfd
Author: l10n daemon script <scripty@kde.org>
Date:   Sat Feb 21 06:33:25 2015 +0000

    SVN_SILENT made messages (after extraction)

commit 94dcdc4280ebb5de032f76f6b8e67f9693941617
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Feb 9 05:43:09 2015 +0000

    SVN_SILENT made messages (after extraction)

commit 6752db06159df563731f83f76e7814aab9bfe889
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Wed Jan 28 20:46:03 2015 +0200

    Fix any initial state of the loan editor represented by group buttons.
    
    Use QAbstractButton::click instead of QAbstractButton::animateClick
    to set the initial state of some values. This is needed because when
    animateClick is used the value is only set after 100 ms thus possibly
    overwriting any initial values set by KEditLoanWizard::loadWidgets
    which is called immediately after creating the wizard pages (thus
    before the click would actually occur).
    
    This fixes the state of loan type ('lending' or 'borrowing') in the
    wizard.
    
    BUG: 343348
    (cherry picked from commit ee9415d83781269fa73afabd900638574c71e4c6)

commit 3c2f0201d39e49dc52a1bb0e649587b3f77fe91a
Author: l10n daemon script <scripty@kde.org>
Date:   Wed Jan 21 06:55:53 2015 +0000

    SVN_SILENT made messages (.desktop file)

commit cf87967b565c35019ee5709dd7b9781e3fef288c
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Jan 17 12:07:29 2015 +0000

    BUG:342926
    - Fix incorrect disabling of Next button in CSV importer.
    
    (cherry picked from commit fef9efb4ec87520ba976726b641b57b30d6f91a7)

commit 7446c81a81f46f60c9fc3da1fe1792084de41cb8
Author: l10n daemon script <scripty@kde.org>
Date:   Tue Jan 13 09:36:06 2015 +0000

    SVN_SILENT made messages (.desktop file)

commit b5af3266f86fa6a93a56483d87c16199cd50828b
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Jan 12 07:09:11 2015 +0000

    SVN_SILENT made messages (.desktop file)

commit 1736d6b4da6404ab7a9ef3b12eb7b14620ff877c
Author: l10n daemon script <scripty@kde.org>
Date:   Sun Jan 11 07:20:17 2015 +0000

    SVN_SILENT made messages (.desktop file)

commit c16da165858b4139e24b5d3d59aac79c1fae8ea0
Author: Christian Dávid <christian-david@web.de>
Date:   Sun Jan 4 14:25:13 2015 +0100

    Changed label for serach bar in scheduled transactions to "Filter"
    
    Before is was "Search" but that is not correct.
    
    REVIEW: 121803
    (cherry picked from commit 8db5ac92498da317f11105759d578c48348e85c2)

commit fc539ad85c99c78dce648020b819d46c534c41af
Author: Christian Dávid <christian-david@web.de>
Date:   Sat Jan 3 13:42:48 2015 +0100

    Changed label for serach bar in ledger from "Search" to "Filter"
    
    The field is actually more filter than search.
    
    This also fixes the bug "In Ledger: Keyboard shortcut for Search (Alt-E)
    does not work".
    
    I could not change the label in scheduled transactions view because it
    is hard coded in the library. But this should be done as well.
    
    BUG: 338296
    FIXED-IN: 4.7.2
    REVIEW: 121803
    (cherry picked from commit bf29b86bcb28c9a1837f351a0cb83f791c779eda)

commit 7ecf237640dc54fe53b91680d30c8c7ba361ded3
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Wed Dec 31 12:04:35 2014 +0100

    Lithuanian Litas is now an ancient currency
    
    On 1 January 2015 the Litas will be switched to the Euro at the rate of
    3.4528 to 1
    
    (cherry picked from commit 84250e2de93b973d0a00ddf4a11cb7fd845c928b)

commit 1fe52ae09343646080c283c93de3f5bd12e0bab7
Author: Thomas Baumgart <thb@net-bembel.de>
Date:   Mon Dec 29 18:02:34 2014 +0100

    Remove Yugoslavian Dinar
    
    The Yugoslavian Dinar is not in use for almost 12 years, so there
    is no need to create it in new files. The replacement in Serbia is
    the Serbian Dinar which is already created for new KMyMoney files.
    If a user uses a file created with a KMyMoney version < 4.5.96 the
    Serbian Dinar (RSD) needs to be created manually.
    
    BUG: 342275
    (cherry picked from commit b3b15ce04d904f63d64b5103ebe725845ddac2dc)

commit 5f85c0074bfd9dd4cbd3743ee492d055f602c570
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Dec 13 17:07:56 2014 +0000

    BUG:330737
    Fix problem in commit 00ec9fa1d3860f7ce4f9d3ba46d8a7a6ba1ee329.
    Selecting tab as field separator resulted in colon being used, even
    though the UI did not present the colon as a possible selection.
    
    Please enter the commit message for your changes. Lines starting
    
    (cherry picked from commit 9f8f78edeba974b01c762bb9c5e75bfd714a85e5)

commit e177bc3875084d06b0f141150ddddbc8726eb82c
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Dec 13 00:09:06 2014 +0000

    BUG:330737
    Fix problem with hard-coded field separators, by adding selection to the UI. The use of a colon as field separator would clash with the category:subcategory use, so is excluded.
    
    Export of Reinv and Div transactions is improved.
    
    (cherry picked from commit 00ec9fa1d3860f7ce4f9d3ba46d8a7a6ba1ee329)

commit d172e4c1339a80d2c2fbc3651fa005a092d8a33d
Author: Allan Anderson <agander93@gmail.com>
Date:   Sat Dec 6 12:35:34 2014 +0000

    BUG: 322381
    Fix fees(taxes) being deducted twice from dividend amount during QIF import.
    Also fix imported amount being negated.
    
    The former appears to be a very long-standing problem.
    
    The latter is a consequence of the fix for BUG 333522 5c54aafef7aa8a39ffd3b5e2dcb6538301652ee5.
    
    Also, remove unwanted comments.
    
    (cherry-picked from e09e3467305aa926ba49852638b44ceb8089c318)

commit f5d48ea7af7cdc6ecf1e94a7cdbb65f57c70ad73
Author: Christian Dávid <christian-david@web.de>
Date:   Mon Dec 15 22:16:31 2014 +0100

    Added currency sign for turkish lira
    
    The sign (U+20BA) was added to the unicode standard in 2012, it should
    be save to use it now.
    
    FEATURE: 296073
    FIXED-IN: 4.7.2

commit 2e770f13c0126d57a18dba0685fb31022dab2d2f
Author: l10n daemon script <scripty@kde.org>
Date:   Fri Nov 28 05:40:47 2014 +0000

    SVN_SILENT made messages (after extraction)

commit ef431608d5fc4a122793c63fb9e863a2191fd739
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sat Nov 22 18:37:40 2014 +0200

    Disable 'Merge' and 'Clear zero' actions while editing splits.
    
    Modifying the split table while it's in edit can cause a crash so
    disable these action while in edit.
    
    BUG: 341040
    (cherry picked from commit bdfaa51c9e85a642e39822a2a378a87d42941558)

commit 1d15adbfcf3faae7d57984ddcf6aec54a3199f9b
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Nov 17 05:16:52 2014 +0000

    SVN_SILENT made messages (after extraction)

commit 4c8c7152eee7e59d2a82448ebc149cb0b35c757a
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Nov 10 05:13:19 2014 +0000

    SVN_SILENT made messages (after extraction)

commit cd6b3c9a653096188097fba254360edda395a14f
Author: Cristian Oneț <onet.cristian@gmail.com>
Date:   Sun Nov 9 12:27:41 2014 +0200

    Fix the missing interest category for 'Buy' activities.
    
    This fix is based on Allan's patch which is attached to the bug report
    that I incorrectly thought to be fixed. I did some refactoring
    because the readability of the investment activities was horrible. We
    really should not have that kind of code in the application.
    
    BUG: 276322
    (cherry picked from commit 404d6279c74b8e93696f774fd04ac55d29307325)

commit 5d714e337cdc5b7d8ad3f9c829254f70ce97695d
Author: l10n daemon script <scripty@kde.org>
Date:   Mon Nov 3 05:15:36 2014 +0000

    SVN_SILENT made messages (after extraction)
