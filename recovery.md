---
title: "The KMyMoney recovery key"
layout: page
---

# The KMyMoney recovery key

KMyMoney has the ability to encrypt your data using GPG.
Besides using your own key for encryption, KMyMoney supports the
ability to encrypt your data with a recover key only available to the
core developers.

This key will have no effect if you always have access to
your own key, but if you lose your key or the passphrase for
it, the KMyMoney recover key comes in handy as now the core
developers can help you to recover your financial data. The only
thing you have to do upfront is to encrypt your data with the
recover key. If you don't do that, There is nothing we can do to help you.

**For the second time I received an email with a request for help to decrypt a
KMyMoney data encrypted with GPG where the user has lost his/her
GPG-Key(ring). Unfortunately, in both cases KMyMoney's recover key feature had not been
used and I could not recover the data.** If I had the ability to help in
these cases (without the recover key) it would mean that I can successfully
break any GPG encryption. Trust me, this is not the case.

**The *only* thing that allows me to help a user in this situation is the
KMyMoney recover key.** Using this feature, you encrypt your data with your
key ***and*** the recover key. In case you lose yours, you can send me the
file and I can use the secret part of the recover key pair to decrypt it.
I will ask you some questions about the contents of the file to make sure
that the person who sends me the file is the legal owner of the file. You
will also have to provide me with your new (public) key, which I will use
to encrypt the file to send it back to you. This way, your data is never
travelling the internet in readable form.

BTW: I encrypt my data using my own key (the same I use to sign all my
e-mails) and the recover key just for this purpose. I have stored a
printed copy of the recover key as well as a CD containing it in a safe at
my bank. This way, I can always reconstruct it and it is not getting lost.

**Now, don't ever tell me you have not used the recover key feature... you
have been warned.**

Import the key into your GPG keyring before you can use it
inside KMyMoney. Select encryption against this key in the
Security settings dialog. Save your data. That's it.


    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Comment: http://www.net-bembel.de/

    mQGiBEHZVEsRBADT6dNItEHaZjpa9OOZ24mbemfwrVbCzH1EuV6mE+7ID8yH37Va
    B5chb3qg44nAc4GY8F9Y/mggTQsG/Lvp6fIj1ADFHygHxeyzr7rh323TLZqaDbkc
    rTQX0mzOK1I1Crp8AaRu3+Rs6J6WMC94QcMBNYyRppg0AN4Hxi3aPdXggwCgjd7a
    wlFkKK4tXubzoXDOH06HDT0EAITZQdcZvlRMXx3XI4B/Qll3hgN55OVVhrV+xY34
    E6XBFY1aaqLbussFCZOOcjUELymcD/eK/qt+mYLdkvmA0IYTk+W3E2NXvewphQuJ
    X9h7kl5/Cvtm/0HLBIeRLEC4TVEruqc2ml7wni24HOC/Ez+vU4Zzk4HZVyS3akRl
    xFnFBAC4fNyrupnWlWPd2UJNyggpd40rUYzy/yK+4W0b/3coHP1hOSFf1RBhhgC3
    D/W8QEAWnCih15IXNNI4om86Bz+p9cnoDVELIjeq5BvafpPGGkz84Z7YU1+q9Lhc
    oXabxZdzI/dC6EjOLThqQL0J/xlRaDyfdWXlHA9Kpf58Qz8w17RJS015TW9uZXkg
    ZW1lcmdlbmN5IGRhdGEgcmVjb3ZlcnkgPGtteW1vbmV5LXJlY292ZXJAdXNlcnMu
    c291cmNlZm9yZ2UubmV0PohkBBMRAgAkBQJB2VRLAhsDBQkJZgGABgsJCAcDAgMV
    AgMDFgIBAh4BAheAAAoJEFmw+CbSsIRATLUAoISpsD7HywfBRxxtTGcoCddFvyot
    AJ9iFoySYwXVz6/Q8W9yqxEGCuhaBYhMBBMRAgAMBQJB2V9SBYMJZfZ5AAoJEJxZ
    20C3XdO6Ka4AoKV1dfZLAfs6IMjBdLlqK4yXwboyAJ4pzfrXOC+5EWnvLWr1jjbU
    ceEkfYhGBBARAgAGBQJGin/0AAoJEDfVOyfVAUYWzbkAoLLFoH5qpTqX4DHqPLhA
    DZhFHRa8AKDjCDTqbEUfMxHwvDol70rB616DqIhGBBARAgAGBQJHdE5/AAoJECm6
    cuUpyqIU1ZwAn0dsvIE+qLRYmTJT9vNNMS5oKviaAKC0ljkJDgf63LwYT8li+C8a
    x1wYGIh7BBMRCgA7AhsDBgsJCAcDAgMVAgMDFgIBAh4BAheAFiEELhsZu95KmtO1
    G8QYWbD4JtKwhEAFAl3svxkFCSHf484ACgkQWbD4JtKwhEDr+ACcDE/KZJT/i+Mp
    rkeqy2Uq8+g19vQAn2c6U33WhheIlManLouBiXtIV8ASiQEcBBABAgAGBQJGfULB
    AAoJEHJCAEvq1TJwKTIH/05RnEECu63km8NZVx8H7CjVkl8jc+kg2xPfz6zdeV53
    fcdHKLrZJEbgdrCdSPg8iiMwD9dklw/o1I9USMcSypk6IUpTJoaK7NoFP+h2eoMT
    28HocWd4fhL/kS+Lz3yElD59ejLUtdmI7IoC6EcWnr4t4WoEkTtJ9BL8nDCyoeOt
    kdBQpUsuLoO5tASg9Ydu/a3sLvxueQF006x1yfkcUw+xZ/WE62xIDV01yVXLZUw3
    xYaZI2Alye0qQ4PJk6nPQGgJArACbRfssERgbOAi9KneToMQmJ7UEkWH4UZz0zo+
    P7zns9JvHQ4o6NfcQUrQDdgq8OSVjp+NCK4IiR2apkWIRgQQEQgABgUCTFQy2AAK
    CRDWp10Ysds7X33FAJ9Ip++rUruA6hPbV7TsEayh/Mg7iQCgh1QtLB7kh1TSWM/C
    KEqGOdmbRnWIZAQTEQIAJAIbAwYLCQgHAwIDFQIDAxYCAQIeAQIXgAUCS0JB+AUJ
    Es7vLQAKCRBZsPgm0rCEQMZZAJ93gkHTFoYpbx0nCc/pvXoLxLelUgCfTYrD9WUY
    6oSUihlTjEELz4DD+cOIZAQTEQIAJAIbAwYLCQgHAwIDFQIDAxYCAQIeAQIXgAUC
    VDE/kgUJHDsrRwAKCRBZsPgm0rCEQMeCAKCMBK0zaqs2L23isA9or+BwIWh1NQCe
    IWOSS+75n3moWF2eM71MVZIv1ji0N0tNeU1vbmV5IGVtZXJnZW5jeSBkYXRhIHJl
    Y292ZXJ5IDxyZWNvdmVyQGtteW1vbmV5Lm9yZz6IgAQTEQoAQAIbAwcLCQgHAwIB
    BhUIAgkKCwQWAgMBAh4BAheAFiEELhsZu95KmtO1G8QYWbD4JtKwhEAFAl3svxkF
    CSHf484ACgkQWbD4JtKwhEAHSgCbBoRECStWV5od70Lu0Py4VpjTUF4AnRqEsr+9
    bM7xJMvoDO/tVE13nSLniEYEExECAAYFAlQxROIACgkQnFnbQLdd07olPgCgkHT6
    /bpLV3lqHy84MK/Drkk6ZlcAoNrTzg9BvRg4K+7U8GoTTPBqlJnYiGkEExECACkF
    AlQxRKECGwMFCRw7K0cHCwkIBwMCAQYVCAIJCgsEFgIDAQIeAQIXgAAKCRBZsPgm
    0rCEQPzpAJ9hDz7is4YlUlQeyR3/xwPjto19pQCdElFQhAV9xFM6gIt042chQpkB
    VcO5Ag0EQdlUVhAIANVkbMJOK9SPu/qJmU1NV+uOZni6uJUjm1nyLqkQPG0+Uv6A
    Ykcd8jjAOjoXVboHglI2l/aACGhuO/EN9IUaK9BK0C+B1XDGKzyXy+Qudepq4y1Q
    SHkaT63FMcHDtaOMff3i1qFBm4GLuQ5icllEHZjZgw8T0TUrOsgbRZsHajoqFmVB
    KoFFvABigqqksNmPU2hcnveHURHg2oNFn3hIstchZJ9ntkeaBEXp+Y+shx2qBBTA
    kWQEHlahwt0x4hFctWr0hCjVLnIcBQtGRK3jBQ413VZNGHlKBrt/436Sw8tgr3W6
    ws1ip/hzhEZpJq6JYeThKNnUScpzHRX3OuEHIp8AAwcH/Rw3xL0Y872XFxyLBAhS
    xFutftFCaVYIEMdNXv9gkp6/sz/LLWXeiRBP4E2YuPYhxXTfZgm/so/Ceyk43hky
    JqRvpHzQOwl3IuIQHplXv98JAWC7+Dtk9r9N783jlUaAoc3Z8TiKBIHJDcj3CbLm
    rMZghyXAAnsUxv2FUMzKTq61JLWO33KlFa3q6k+FTWx8xUEb9TRjoHCSQmWmqLpo
    cP0LH2eRY0CB4I0JrrZSYTZL/0YFfogFD1vM60bUDeQ3DZZPKWDTBuFiBgAbEiRc
    NbiNkj/61V3Y7Sxp4nn4TTW9qClNYjyqAqepot7cXmeazgFnP+1PQ+vIqMDnNEQv
    lxKITwQYEQIADwUCQdlUVgIbDAUJCWYBgAAKCRBZsPgm0rCEQKixAJwKc1lQN9f2
    IeWqe91qlv7NT4radACfXqgAN8VL3w2NuoCxpdQR6nWkdfOIZgQYEQoAJgIbDBYh
    BC4bGbveSprTtRvEGFmw+CbSsIRABQJd7L+/BQkh3+RpAAoJEFmw+CbSsIRAW50A
    n2EAldLeBkpIBhl6K6nghiX+5EEXAKCDAErqDWMCRdoBPDmmSjj3rqt6gg==
    =TpDx
    -----END PGP PUBLIC KEY BLOCK-----

