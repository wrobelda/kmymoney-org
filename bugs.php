<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
# 
# common wrapper for showing bug lists from bug.kde.org 
#
# syntax:
#  bugs.php?<type>/<version>
#
# <type> - references the $urls array keys in lib.inc
# <version> - version to show e.g 4.8.1 for the related version
#             or 4.8 for all versions in this branch
#
	include(dirname(__FILE__)."/lib.inc");
	$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	if (strpos($query,'/') !== FALSE)
		list($type, $version) = explode('/',$query);
	else {
		$type = $query;
		$version = "";
	}
	$url = buglist($type, $version);
	if ($url)
		Header("Location: $url");
?>