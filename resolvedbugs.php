<?php
#
# @author Ralf Habacker <ralf.habacker@freenet.de>
#
# redirect to bugs.kde.org and shows a list of resolved bugs
#
# syntax:
#   resolvedbugs.php           - shows all resolved bugs
#   resolvedbugs.php?<version> - shows resolved bugs of <version> e.g. 4.8.1
#                                or 4.8 for all releases of the mentioned branch
#
	include(dirname(__FILE__)."/lib.inc");
	$version = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : "";
	$url = buglist('resolvedbugs',$version);
	Header("Location: $url");
?>