---
title: '        KMyMoney goes cross platform
      '
date: 2016-06-15 00:00:00 
layout: post
---

<p>As a result of the development towards KDE Frameworks 5 (KF5), KMyMoney has been successfully build by the developers from a common source base on the following platforms today:
          <ul>
            <li>Linux (various distros) using GCC and CLANG</li>
            <li>Mac OS X using CLANG</li>
            <li>Microsoft Windows 7 using MSVC 2015.</li>
          </ul>
        This is very exciting news because the last versions for these platforms could not be build at all lately. The Randa Meetings 2016 was the base for this success which has been accomplished by various platform teams and the KMyMoney developers in a joint effort.</p><p>Thanks to all of you who made this happen.</p><p>The KMyMoney Development Team</p>