---
title: '        Stable release for KDE platform 4
      '
date: 2010-08-16 00:00:00 
layout: post
---

<p>The KMyMoney is pleased to announce the release of a stable version for KDE Platform 4. With over 15 months of development, this new version is the starting point of a new series of KMyMoney versions to leverage the stellar features offered by the KDE Platform.</p><p>Throughout this effort, our focus has been to maintain feature-parity with previous versions, maintaining and improving the high level of quality that your personal financial data deserve. This version has gone through an intensive testing, allowing us to find and fix many bugs and improve the usability of the application.</p><p>Along the way, we did manage to add some new features:</p><ul>
          <li>Better documentation</li>
          <li>Works with the latest version of AqBanking</li>
          <li>Improved usability of the online banking features</li>
          <li>Uses KWallet to store online banking passwords</li>
          <li>The consistency checks runs automatically before saving your data, it now checks for a wider range of problems, and it automatically corrects many of them.</li>
          <li>Runs in all operating systems supported under KDE</li>
        </ul><p>For the last year, our team has been fully committed to release this version. Now, we will continue to make KMyMoney the best personal finance manager by leveraging all features provided by the KDE Platform, and also improving the integration with other applications via Alkimia, the nascent framework of the KDE Finance applications group. Expect interesting features coming your way soon!</p>