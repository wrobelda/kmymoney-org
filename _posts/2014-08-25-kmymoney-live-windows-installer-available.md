---
title: '        KMyMoney Live Windows installer available
      '
date: 2014-08-25 00:00:00 
layout: post
---

<p>We are pleased to announce the immediate availability of the first installer
          from the <b>live build</b> series for <b>Windows</b> <a class="" href="http://sourceforge.net/projects/kmymoney2/files/KMyMoney-Windows/live/kmymoney-x86-setup-4.6.90-cdd451fe35.exe/download">KMyMoney version 4.6.90-cdd451fe35</a>. A <b>live build</b> package is
           created directly from the sources in git master available at that moment.</p><p>Notes about this package:</p><ul>
          <li>it will only run on Windows 7 or newer version</li>
          <li>it uses KDE 4.12.5 and Qt 4.8.6</li>
          <li>it does not yet contain translations</li>
          <li>GPG works with gpg4win out of the box</li>
          <li>as with previous versions it does not contain the HBCI KBanking plugin because AqBanking's build system is autotools based making it hard to build using MSVC</li>
          <li>the OFX import plugin is available</li>
          <li>as with previous versions the Finance::Quote module will only work if you install perl (with the Finance::Quote module) separately</li>
          <li>it will be periodically updated as issues are fixed</li>
        </ul><p>If you currently use KMyMoney on Windows there is no need to uninstall your current version since this version will install in it's own folder and will have it's own shortcut by default. Just remember, the newer version extends the stored information in the data file (like tags) so when switching back to the old version this new extra information (tags), that the old version knows nothing about, will be lost.</p><p>If you find the new version useful feel free to use it on a daily basis since it will be receiving regular updates.</p><p>The KMyMoney Development Team</p>