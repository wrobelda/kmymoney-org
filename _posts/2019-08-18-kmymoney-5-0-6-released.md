---
title: '        KMyMoney 5.0.6 released
      '
date: 2019-08-18 00:00:00 
layout: post
---

<a href="https://download.kde.org/stable/kmymoney/5.0.6/src/kmymoney-5.0.6.tar.xz.mirrorlist">KMyMoney version 5.0.6</a> is now available.

Please take a look at the <a href="changelogs/kmymoney-5-0-6-release-notes.html">release notes</a> before installing.

For a full list of the changes please check out the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.6.txt">changelog</a>

The KMyMoney Development Team
