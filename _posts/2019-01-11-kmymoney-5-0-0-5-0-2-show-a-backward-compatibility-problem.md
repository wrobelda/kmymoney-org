---
title: '        KMyMoney 5.0.0-5.0.2 show a backward compatibility problem
      '
date: 2019-01-11 00:00:00 
layout: post
---

<p>Today the development team discovered a backward compatibility problem that affects customized reports of the
          Information group. If you upgrade from earlier versions of the application, your customization will partially get lost
          as soon as you save the file with the 5.0 version. All other report types are not affected by this problem. The team
          is working on a solution which will be made available with KMyMoney 5.0.3. Unfortunately, there is no workaround.</p><p>For details, please take a look at <a href="https://bugs.kde.org/show_bug.cgi?id=403068">the respective bug tracker entry</a>.</p><p>The KMyMoney Development Team</p>