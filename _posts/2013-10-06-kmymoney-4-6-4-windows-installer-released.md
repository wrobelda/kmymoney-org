---
title: '        KMyMoney 4.6.4 Windows installer released
      '
date: 2013-10-06 00:00:00 
layout: post
---

<p>We are pleased to announce the immediate availability of the <a href="http://sourceforge.net/projects/kmymoney2/files/KMyMoney-Windows/4.6.4/kmymoney-x86-setup-4.6.4.exe/download">KMyMoney 4.6.4 standalone installer</a> for Windows.</p><p>The package contains:<br />
          <strong>KMyMoney 4.6.4</strong> (all available languages)<br />
          <strong>KDE 4.10.5</strong></p><p>Notes about this version:</p><ul>
          <li>it fixes IME (input method editor) related crashes</li>
          <li>the availability of the local user manual was fixed</li>
          <li>as with previous versions the Finance::Quote module will only work if you install perl (with the Finance::Quote module) separately</li>
        </ul><p>As with the other versions KMyMoney 4.6.4 can be installed in a separate directory and tested. But make sure that the KDE platform services of older versions are stopped by issuing the following command</p><pre>C:\Program Files\KMyMoney\bin\kdeinit4.exe --shutdown"</pre><p>We highly recommend it over the previous 4.6.3 version since it's based on an improved KDE platform and a fixed version of KMyMoney. If you are happy with it you can safely remove the previous installation (make sure that you keep your data file).</p>