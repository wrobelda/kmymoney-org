---
title: '        KMyMoney 4.6.3 released
      '
date: 2012-09-10 00:00:00 
layout: post
---

<p>The KMyMoney Team is pleased to announce the immediate availability of KMyMoney version 4.6.3. This version contains quite a few fixes for bugs found in 4.6.2 since it was released seven months ago.</p><p>Here is a list of the most important changes since the last stable release:</p><ul>
          <li>The online statement balance is highlighted if it's different from current file balance</li>
          <li>Correct the post date of the opening balance transaction when the opening date of the account changes</li>
          <li>The header state (adjusted column sizes) is now always restored correctly</li>
          <li>Can be built with Qt 4.8 without patching KDChart</li>
          <li>Fix a crash on opening SQLite databases</li>
          <li>MZN as new Mozambique Metical as well as an entry for MZM as the old Mozambique Metical were added</li>
          <li>Attempt to reconnect to the database (once) on accidental disconnects</li>
          <li>Allow the creation of 'Equity' accounts when "Show equity accounts" is checked</li>
          <li>In the 'Find transactions' dialog trigger a search when return is pressed and there is text entered in the filter</li>
          <li>Reports related fixes</li>
          <li>Fixed a large minimum ledger size when using some fonts</li>
          <li>Fixed SQL sintax in a certain usecase</li>
          <li>CSV importer plugin related fixes</li>
        </ul>