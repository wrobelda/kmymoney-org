---
title: '        KMyMoney 4.7.2 released
      '
date: 2015-04-25 00:00:00 
layout: post
---

<p><a href="http://download.kde.org/stable/kmymoney/4.7.2/src/kmymoney-4.7.2.tar.xz.mirrorlist">KMyMoney version 4.7.2</a> is now available. This version contains fixes for several bugs, here is a short list:
          <ul>
            <li>fix a crash in the transaction split editor</li>
            <li>added the monetary symbol of the Turkish lira</li>
            <li>matching restrictions on an already imported transaction were removed</li>
          </ul>
        The full list of solved issues can be viewed in <a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&amp;f1=cf_versionfixedin&amp;o1=equals&amp;product=kmymoney&amp;query_format=advanced&amp;resolution=FIXED&amp;v1=4.7.2">KDE's issue tracker</a>.</p><p>Thanks to the KDE translation teams the following new translations were added: documentation in Italian and application translation in Slovak, Turkish and Simplified Chinese</p><p>If you are using <a href="http://www.aquamaniac.de/sites/aqbanking/index.php">AqBanking</a> to place transactions with your bank and want to contribute to the availability of this feature in KMyMoney please <a href="mailto:kmymoney-devel@kde.org?subject=Help%20test%20onlinebanking">contact the developers</a> or <a href="https://techbase.kde.org/Projects/KMyMoney#Installation_steps">build git master</a> to give it a try.</p><p>For a full list of the changes please check out the <a href="changelogs/ChangeLog-4.7.2.txt">changelog</a>. We highly recommend upgrading to 4.7.2.</p><p>The KMyMoney Development Team</p>