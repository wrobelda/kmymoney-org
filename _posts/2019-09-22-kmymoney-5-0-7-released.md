---
title: 'KMyMoney 5.0.7 released'
date: 2019-09-22 00:00:00 
layout: post
---

<a href="https://download.kde.org/stable/kmymoney/5.0.7/src/kmymoney-5.0.7.tar.xz.mirrorlist">KMyMoney version 5.0.7</a> is now available. It provides online banking access according to the new PSD2 regulations.

Please take a look at the <a href="/changelogs/kmymoney-5-0-7-release-notes.html">release notes</a> before installing.

For a full list of the changes please check out the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.7.txt">changelog</a>.

Binary packages for 32 and 64 bit Windows are available from the KDE <a href="http://download.kde.org/stable/kmymoney/5.0.7/" class="piwik_link">download mirror network</a>.

The KMyMoney Development Team
