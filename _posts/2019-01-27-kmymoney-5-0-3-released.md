---
title: '        KMyMoney 5.0.3 released
      '
date: 2019-01-27 00:00:00 
layout: post
---

<p><a href="https://download.kde.org/stable/kmymoney/5.0.3/src/kmymoney-5.0.3.tar.xz.mirrorlist">KMyMoney version 5.0.3</a> is now available.</p><p>Please take a look at the <a href="https://kmymoney.org/release-notes.php">release notes</a> before installing.</p><p>The KMyMoney Development Team</p>