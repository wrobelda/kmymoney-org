---
title: '        KMyMoney installer for Windows
      '
date: 2013-01-21 00:00:00 
layout: post
---

<p>We are happy to announce the release of a standalone installer for Windows of KMyMoney version 4.6.3. For a while now the latest version that was available on Windows was 4.6.2. Since we took over the packaging of the application on Windows with the help of the <a href="http://windows.kde.org/" rel="nofollow">KDE on Windows project</a> we will be able to provide the latest stable release once we release a source tarball.</p><p>The package contains:<br />
          KMyMoney 4.6.3 (all available languages)<br />
          KDE 4.8.5</p><p>Please note that the application is far from being thoroughly tested on this platform so please <a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product=kmymoney." rel="nofollow">report any issues you find.</a> We are counting on the community of KMyMoney users on Windows to fix issues that are specific to this platform. Don't forget to use the backup feature (without checking the 'mount' flag).</p><p>Please take care of <a href="http://techbase.kde.org/Projects/KMyMoney#Obtain_a_useful_backtrace_on_Windows" rel="nofollow">these steps</a> before trying to report a crash of the application.</p><p>Known issues of this version:</p><ul>
          <li>the local user manual is not yet available (there is an <a href="http://docs.kde.org/stable/en/extragear-office/kmymoney/index.html">online version</a>)</li>
          <li>the Finance::Quote module will only work if you install perl (with the Finance::Quote module) separately</li>
        </ul><p>If you were already using version 4.6.2 on Windows you can install 4.6.3 in a different directory try it, and if you are happy with it you can safely remove the previous installation (make sure that you keep your data file).</p><p>The KMyMoney Development Team</p>