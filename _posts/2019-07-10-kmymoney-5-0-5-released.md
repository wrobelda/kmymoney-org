---
title: '        KMyMoney 5.0.5 released
      '
date: 2019-07-10 00:00:00 
layout: post
---

<a href="https://download.kde.org/stable/kmymoney/5.0.5/src/kmymoney-5.0.5.tar.xz.mirrorlist">KMyMoney version 5.0.5</a> is now available.

Please take a look at the <a href="https://kmymoney.org/release-notes.php">release notes</a> before installing.

For a full list of the changes please check out the <a href="https://kmymoney.org/changelogs/ChangeLog-5.0.5.txt">changelog</a>.

The KMyMoney Development Team
