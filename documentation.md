---
title: The KMyMoney user manuals
layout: page
---

The user manuals are hosted by [docs.kde.org](https://docs.kde.org).

[KMyMoney (en)](https://docs.kde.org/stable5/en/extragear-office/kmymoney/index.html) [ (pdf)](https://docs.kde.org/stable5/en/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (de)](https://docs.kde.org/stable5/de/extragear-office/kmymoney/index.html) [ (pdf)](https://docs.kde.org/stable5/de/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (es) (outdated)](https://docs.kde.org/stable4/es/extragear-office/kmymoney/index.html)

<!-- https://docs.kde.org/stable4/es/extragear-office/kmymoney/kmymoney.pdf"> (pdf) -->

[KMyMoney (et) (outdated)](https://docs.kde.org/stable4/et/extragear-office/kmymoney/index.html)
<!-- https://docs.kde.org/stable4/et/extragear-office/kmymoney/kmymoney.pdf"> (pdf) -->

[KMyMoney (fr)](https://docs.kde.org/stable4/fr/extragear-office/kmymoney/index.html) 
[ (pdf)  (outdated)](https://docs.kde.org/stable4/fr/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (it)](https://docs.kde.org/stable5/it/extragear-office/kmymoney/index.html) 
[ (pdf)](https://docs.kde.org/stable5/it/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (nl)](https://docs.kde.org/stable5/nl/extragear-office/kmymoney/index.html)
[ (pdf)](https://docs.kde.org/stable5/nl/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (pt)](https://docs.kde.org/stable4/pt/extragear-office/kmymoney/index.html)
[ (pdf)   (outdated)](https://docs.kde.org/stable4/pt/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (pt_BR)](https://docs.kde.org/stable5/pt_BR/extragear-office/kmymoney/index.html)
[ (pdf)](https://docs.kde.org/stable5/pt_BR/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (sv)](https://docs.kde.org/stable5/sv/extragear-office/kmymoney/index.html)
[ (pdf)](https://docs.kde.org/stable5/sv/extragear-office/kmymoney/kmymoney.pdf)

[KMyMoney (uk)](https://docs.kde.org/stable5/uk/extragear-office/kmymoney/index.html)
[ (pdf)](https://docs.kde.org/stable5/uk/extragear-office/kmymoney/kmymoney.pdf)

