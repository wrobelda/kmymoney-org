---
title : "KMyMoney - Support"
layout: page
---

# How can we help You?

For new users, it is recommended to read the [user's manual](documentation.html). Many frequently asked questions are collected in the [FAQ](faq.html).
Should there still be any questions, you should take a look at the
[Forum](https://forum.kde.org/viewforum.php?f=69) or the
[Mailing List](https://mail.kde.org/mailman/listinfo/kmymoney). If you just have a little question you could also use the
[IRC Webchat](http://webchat.freenode.net/?channels=kmymoney).



* [**FAQ**](faq.html) Here you can find *Frequently Asked Questions*.
  If you have a question that is not answered on this FAQ, please do not hesitate to
  post it on the KMyMoney Developers List or KMyMoney Users List and we will try to
  get an answer for you as soon as possible.

* [**User's Manual**](documentation.html) Here you can visit the documentation to read about all the functions KMyMoney provides, maybe even in your own language.

* [**Mailing List**](https://mail.kde.org/mailman/listinfo/kmymoney) Do you want to subscribe the KDE mailing list or send an E-Mail to all List Members of kmymoney.org? Then you are right here. An [archive](https://marc.info/?l=kmymoney) of mails to this list is also available.

* [**Forum**](https://forum.kde.org/viewforum.php?f=69) In the Forum for KMyMoney it is possible to write about an existing topic or create a new topic.

* [**IRC Webchat**](https://webchat.freenode.net/?channels=kmymoney)

* [**IRC Channel**](irc://irc.freenode.net/kmymoney)

* [**Report a Bug**](https://bugs.kde.org/enter_bug.cgi?product=kmymoney&format=guided) Did you find some bugs? Please report them in the KDE Bugtracking System for kmymoney.org

* [**Suggest a Feature**](https://bugs.kde.org/enter_bug.cgi?product=kmymoney&format=guided) Are you missing some functionality? Please open a wishlist item in the KDE Bugtracking System for kmymoney.org and make sure it is not already listed.

* [**Userbase Wiki**](https://userbase.kde.org/KMyMoney) That's the Wiki for the users. The Wiki for the programmers is called [Techbase Wiki](https://techbase.kde.org/Projects/KMyMoney).

* [**Recovery key**](recovery.html) To prevent accidental lockout from your encrypted KMyMoney file you can use the recovery key.

